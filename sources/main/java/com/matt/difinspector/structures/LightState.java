package com.matt.difinspector.structures;

public class LightState
{
	private byte red, green, blue;
	private int activeTime;
	private int dataIndex;
	private short dataCount;
	
	public LightState(byte red, byte green, byte blue, int activeTime, int dataIndex, short dataCount)
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.activeTime = activeTime;
		this.dataIndex = dataIndex;
		this.dataCount = dataCount;
	}
	
	public byte getRed()
	{
		return this.red;
	}
	
	public byte getGreen()
	{
		return this.green;
	}
	
	public byte getBlue()
	{
		return this.blue;
	}
	
	public int getActiveTime()
	{
		return this.activeTime;
	}
	
	public int getDataIndex()
	{
		return this.dataIndex;
	}
	
	public short getDataCount()
	{
		return this.dataCount;
	}
	
	@Override
	public String toString()
	{
		return "(LightState){red: " + this.red + ", green: " + this.green + ", blue: " + this.blue + ", activeTime: " + this.activeTime + ", dataIndex: " + this.dataIndex + ", dataCount: " + this.dataCount + "}";
	}
}
