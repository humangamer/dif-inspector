package com.matt.difinspector.structures;

public class AnimatedLight
{
	private int nameIndex;
	private int stateIndex;
	private short stateCount;
	private short flags;
	private int duration;
	
	public AnimatedLight(int nameIndex, int stateIndex, short stateCount, short flags, int duration)
	{
		this.nameIndex = nameIndex;
		this.stateIndex = stateIndex;
		this.stateCount = stateCount;
		this.flags = flags;
		this.duration = duration;
	}
	
	public int getNameIndex()
	{
		return this.nameIndex;
	}
	
	public int getStateIndex()
	{
		return this.stateIndex;
	}
	
	public short getStateCount()
	{
		return this.stateCount;
	}
	
	public short getFlags()
	{
		return this.flags;
	}
	
	public int getDuration()
	{
		return this.duration;
	}
	
	@Override
	public String toString()
	{
		return "(AnimatedLight){nameIndex: " + this.nameIndex + ", stateIndex: " + this.stateIndex + ", stateCount: " + this.stateCount + ", flags: " + this.flags + ", duration: " + this.duration + "}";
	}
}
