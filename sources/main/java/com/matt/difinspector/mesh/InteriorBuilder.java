package com.matt.difinspector.mesh;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.interior.InteriorSimpleMesh;
import com.matt.difinspector.interior.InteriorSubObject;
import com.matt.difinspector.main.DifInspector;
import com.matt.difinspector.materials.MaterialList;
import com.matt.difinspector.materials.TexData;
import com.matt.difinspector.materials.TexGenList;
import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.AnimatedLight;
import com.matt.difinspector.structures.BSPNode;
import com.matt.difinspector.structures.BSPSolidLeaf;
import com.matt.difinspector.structures.ColorI;
import com.matt.difinspector.structures.ConvexHull;
import com.matt.difinspector.structures.CoordBin;
import com.matt.difinspector.structures.Face;
import com.matt.difinspector.structures.ItrPaddedPoint;
import com.matt.difinspector.structures.LightState;
import com.matt.difinspector.structures.LightStateData;
import com.matt.difinspector.structures.NullSurface;
import com.matt.difinspector.structures.Portal;
import com.matt.difinspector.structures.Surface;
import com.matt.difinspector.structures.TexGenPlanes;
import com.matt.difinspector.structures.TexMatrix;
import com.matt.difinspector.structures.TriFan;
import com.matt.difinspector.structures.Zone;
import com.matt.difinspector.util.ImageData;
import com.matt.difinspector.util.Util;
import com.matt.jmatt.lists.OptimizedList;

public class InteriorBuilder
{
	protected MeshBuilder builder;
	
	public InteriorBuilder(String defaultTexture)
	{
		this.builder = new MeshBuilder(defaultTexture);
	}
	
	public void addVertex(Point3F vertex, Point2F uv, String texture)
	{
		this.builder.addVertex(vertex, uv, texture);
	}
	
	public void addVertex(Point3F vertex, Point2F uv)
	{
		this.builder.addVertex(vertex, uv);
	}
	
	public void addVertex(Point3F vertex)
	{
		this.builder.addVertex(vertex);
	}
	
	public Interior build()
	{
		DifInspector inspector = DifInspector.getInstance();
		inspector.displayProgress("Building Mesh", 0);
		
		Mesh mesh = builder.build();
		Interior result = new Interior();
		
		inspector.displayProgress("Adding Points", 1);
		
		//System.out.println("Adding Points");
		ItrPaddedPoint[] points = new ItrPaddedPoint[mesh.getVertices().length];
		for (int i = 0; i < points.length; i++)
			points[i] = new ItrPaddedPoint(mesh.getVertices()[i]);
		result.setPoints(points);
		
		inspector.displayProgress("Adding Windings", 6);
		
		//System.out.println("Adding Windings");
		result.setWindings(mesh.getIndices());
		
		inspector.displayProgress("Adding MaterialList", 8);
		
		//System.out.println("Adding MaterialList");
		MaterialList materialList = new MaterialList();
		for (String texture : mesh.getTextures())
			materialList.addMaterial(texture);
		result.setMaterialList(materialList);
		
		int[] textureIndices = mesh.getTextureIndices();
		
		inspector.displayProgress("Adding TexGenEQs", 10);
		
		//System.out.println("Adding TexGenEQs");
		OptimizedList<Point3F> vertices = new OptimizedList<Point3F>(mesh.getVertices(), mesh.getIndices());
		OptimizedList<Point2F> texCoords = new OptimizedList<Point2F>(mesh.getTextureCoords(), mesh.getTexCoordIndices());
		OptimizedList<TexGenPlanes> texGenEQs = new OptimizedList<TexGenPlanes>();
		
		OptimizedList<Point3F> normals = new OptimizedList<Point3F>();
		
		for (int i = 0; i < vertices.sizeIndices(); i += 3)
		{
			Point3F p1 = vertices.getObject(i+0);
			Point3F p2 = vertices.getObject(i+1);
			Point3F p3 = vertices.getObject(i+2);
			
			Point3F normal = new PlaneF(p1, p2, p3);
			normals.add(normal);
			normals.add(normal);
			normals.add(normal);
		}
		
		Map<String, ImageData> images = new HashMap<String, ImageData>();
		for (String texture : mesh.getTextures())
		{
			images.put(texture, ImageData.loadImage("textures/" + texture));
		}
		
		TexGenPlanes lastTexGen = null;
		
		for (int i = 0; i < texCoords.sizeIndices(); i ++)
		{
			if (i + 2 >= texCoords.sizeIndices())
			{
				System.out.println("Extra UV?");
				int dif = i - texCoords.sizeIndices();
				for (int j = 0; j < dif; j++)
					texGenEQs.add(lastTexGen);
				break;
			}
			Point2F uv0 = texCoords.getObject(i+0);
			Point2F uv1 = texCoords.getObject(i+1);
			Point2F uv2 = texCoords.getObject(i+2);
			Point3F point0 = vertices.getObject(i+0);
			Point3F point1 = vertices.getObject(i+1);
			Point3F point2 = vertices.getObject(i+2);
			
			Point3F normal = normals.getObject(i);
			
			String texture = mesh.getTextures()[textureIndices[i]];
			
			// old uv stuff
			/*Point2F offset = new Point2F(0, 0);
			Point2F scale = new Point2F(0.25f, 0.25f);
			float rotation = 0;
			if (texture.equalsIgnoreCase("edge_white") || texture.equalsIgnoreCase("edge_white_mbm"))
				scale = new Point2F(0.125f, 0.125f);
			if (texture.equalsIgnoreCase("plate_1"))
				scale = new Point2F(0.125f, 0.125f);
			if (texture.equalsIgnoreCase("stripe_caution") || texture.equalsIgnoreCase("stripe_caution_mbm"))
				rotation = 45;
			if (texture.equalsIgnoreCase("beam"))
				rotation = 90;
				//offset = new Point2F(2, 0);*/
			//TexData texData = new TexData(normal, offset, rotation, scale);
			//ImageData imageData = images.get(texture);
			//TexGenPlanes texGenPlanes = new TexGenPlanes();
			//if (imageData != null)
			
			TexGenPlanes texGenPlanes = Util.getTexGen(point0, point1, point2, uv0, uv1, uv2);
				//texGenPlanes = Util.getTexGen(texData, imageData);
			//TexGenPlanes texGenPlanes = Util.getTexGen(point0, point1, point2, uv0, uv1, uv2); //new TexGenPlanes(normal, point, uv);
			lastTexGen = texGenPlanes;
			texGenEQs.add(texGenPlanes);
			//texGenEQs.add(texGenPlanes);
			//texGenEQs.add(texGenPlanes);
		}
		
		TexGenList texGenList = new TexGenList();
		for (TexGenPlanes planes : texGenEQs.getObjects())
			texGenList.add(planes);
		result.setTexGenEQs(texGenList);
		
		inspector.displayProgress("Processing Faces", 15);
		
		//System.out.println("Processing Faces");

		OptimizedList<Face> faces = new OptimizedList<Face>();
		
		for (int i = 0; i < vertices.sizeIndices(); i += 3)
		{
			if (i >= vertices.sizeIndices())
			{
				int extra = i - vertices.sizeIndices();
				System.out.println("Extra Points: " + extra);
				break;
			}
			
			Point3F p1;
			Point3F p2;
			Point3F p3;
			
			//if (i % 2 == 0)
			//{
				p1 = vertices.getObject(i+0);
				p2 = vertices.getObject(i+1);
				p3 = vertices.getObject(i+2);
			//} else {
			//	p1 = vertices.getObject(i+2);
			//	p2 = vertices.getObject(i+1);
			//	p3 = vertices.getObject(i+0);
			//}
			
			
			String texture = mesh.getTextures()[textureIndices[i]];
			
			Face face = new Face(p1, p2, p3, texture, texGenEQs.getObject(i));
			
			// temporary texture stuff
			/*float xs;
			float ys;
			
			if (texture.equalsIgnoreCase("edge_white"))
			{
				xs = 0.125f;
				ys = 0.125f;
			} else {
				xs = 0.25f;
				ys = 0.25f;
			}
			TexData data = new TexData(face.getPlane(), new Point2F(0, 0), 0, new Point2F(xs, ys));
			ImageData texture1 = ImageData.loadImage("textures/" + texture);
			TexGenPlanes planes;
			if (texture1 == null)
			{
				planes = new TexGenPlanes();
			} else {
				planes = Util.getTexGen(data, texture1);
			}
			texGenEQs.add(planes);
			face.setTexGen(planes);*/
			
			faces.add(face);
		}
		
		//temp
		TexGenList texGenList2 = new TexGenList();
		for (TexGenPlanes planes : texGenEQs.getObjects())
			texGenList2.add(planes);
		result.setTexGenEQs(texGenList2);
		
		/*System.out.print("Windings: ");
		for (int i = 0; i < mesh.getIndices().length; i++)
		{
			System.out.print(mesh.getIndices()[i] + ", ");
		}
		System.out.println();
		
		System.out.print("Face Windings: ");
		for (int i = 0; i < faces.sizeIndices(); i++)
		{
			TriangleF tri = faces.getObject(i).getTriangle();
			int point1 = vertices.indexOfObject(tri.getPoint1());
			int point2 = vertices.indexOfObject(tri.getPoint2());
			int point3 = vertices.indexOfObject(tri.getPoint3());
			System.out.print(point1 + ", " + point2 + ", " + point3 + ", ");
		}
		System.out.println();*/
		
		inspector.displayProgress("Adding Planes", 17);
		
		//System.out.println("Adding Planes");
		List<Face> facesObjs = faces.getObjects();
		OptimizedList<PlaneF> planes = new OptimizedList<PlaneF>();
		for (Face f : facesObjs)
		{
			planes.add(f.getPlane());
		}
		result.setPlanes(planes.getObjects());
		
		inspector.displayProgress("Adding Plane Normals", 20);
		
		//System.out.println("Adding Plane Normals");
		Point3F[] planeNormals = new Point3F[planes.sizeIndices()];
		for (int i = 0; i < planes.sizeIndices(); i++)
			planeNormals[i] = planes.getObject(i);
		result.setPlaneNormals(planeNormals);
		
		inspector.displayProgress("Adding Surfaces", 21);
		
		//System.out.println("Adding Surfaces");
		OptimizedList<Surface> surfaces = new OptimizedList<Surface>();
		
		OptimizedList<String> textures = new OptimizedList<String>(mesh.getTextures(), mesh.getTextureIndices());
		
		int ind = 0;
		for (Face face : faces)
		{
			//TriangleF triangle = face.getTriangle();
			int windingStart = ind * 3;//vertices.indexOfIndex(triangle.getPoint1());
			int windingCount = 3;
			short planeIndex = (short)planes.indexOfObject(face.getPlane());
			short textureIndex = (short)textures.indexOfObject(face.getTexture());
			int texGenIndex = texGenEQs.indexOfObject(face.getTexGen());
			short lightCount = 0;
			byte surfaceFlags = 16;
			int fanMask = 15;
			int lightStateInfoStart = 0;
			int mapOffsetX = 0;
			int mapOffsetY = 0;
			int mapSizeX = 0;
			int mapSizeY = 0;
			boolean unused = false;
			int brushId = 0;
			
			Surface surface = new Surface(windingStart, planeIndex, textureIndex, texGenIndex, lightCount, surfaceFlags, windingCount, fanMask, lightStateInfoStart, mapOffsetX, mapOffsetY, mapSizeX, mapSizeY, unused, brushId);
			surfaces.add(surface);
			
			ind++;
		}
		result.setSurfaces(surfaces.getObjects());
		
		inspector.displayProgress("Adding PointVisibilities", 25);
		
		//System.out.println("Adding PointVisibilities");
		byte[] pointVisibilities = new byte[points.length];
		for (int i = 0; i < points.length; i++)
		{
			pointVisibilities[i] = -1;
		}
		result.setPointVisibility(pointVisibilities);
		
		inspector.displayProgress("Adding Winding Indices", 27);
		
		//System.out.println("Adding Winding Indices");
		result.setWindingIndices(new TriFan[0]);
		
		inspector.displayProgress("Adding Zones", 28);
		
		//System.out.println("Adding Zones");
		Zone[] zones = new Zone[1];
		zones[0] = new Zone((short)0, (short)0, 0, 0, (short)surfaces.sizeIndices(), (short)0, 0, 0, (short)0, (short)0);
		result.setZones(zones);
		
		inspector.displayProgress("Adding Zone Surfaces", 29);
		
		//System.out.println("Adding Zone Surfaces");
		short[] zoneSurfaces = new short[surfaces.sizeIndices()];
		for (short i = 0; i < surfaces.sizeIndices(); i++)
			zoneSurfaces[i] = i;
		result.setZoneSurfaces(zoneSurfaces);
		
		inspector.displayProgress("Adding Zone Static Meshes", 30);
		
		//System.out.println("Adding Zone Static Meshes");
		result.setZoneStaticMeshes(new int[0]);
		
		inspector.displayProgress("Adding Zone Portal List", 31);
		
		//System.out.println("Adding Zone Portal List");
		result.setZonePortalList(new short[0]);
		
		inspector.displayProgress("Adding Portals", 32);
		
		//System.out.println("Adding Portals");
		result.setPortals(new Portal[0]);
		
		inspector.displayProgress("Adding NormalLMapIndices", 35);
		
		//System.out.println("Adding NormalLMapIndices");
		// NormalLMapIndices may be more complicated
		int[] normalLMapIndices = new int[surfaces.sizeIndices()];
		for (int i = 0; i < surfaces.sizeIndices(); i++)
			normalLMapIndices[i] = 0;//i;
		result.setNormalLMapIndices(normalLMapIndices);
		
		inspector.displayProgress("Adding AlarmLMapIndices", 40);
		
		//System.out.println("Adding AlarmLMapIndices");
		int[] alarmLMapIndices = new int[surfaces.sizeIndices()];
		for (int i = 0; i < surfaces.sizeIndices(); i++)
			alarmLMapIndices[i] = 255;
		result.setAlarmLMapIndices(alarmLMapIndices);
		
		inspector.displayProgress("Adding NullSurfaces", 45);
		
		//System.out.println("Adding NullSurfaces");
		// TODO: Null Surfaces
		result.setNullSurfaces(new NullSurface[0]);
		
		inspector.displayProgress("Adding LightMaps", 46);
		
		//System.out.println("Adding LightMaps");
		// TODO: Light Maps
		BufferedImage lightMap = new BufferedImage(256, 256, BufferedImage.TYPE_INT_RGB);
		for (int i = 0; i < 256; i++)
		{
			for (int j = 0; j < 256; j++)
			{
				lightMap.setRGB(i, j, 0x000000);
			}
		}
		result.setLightMaps(new BufferedImage[]{lightMap});
		result.setLightDirMaps(new BufferedImage[]{lightMap});
		result.setLightMapKeep(new boolean[]{false});
		
		inspector.displayProgress("Adding Solid Leaf Surfaces", 50);
		
		//System.out.println("Adding Solid Leaf Surfaces");
		// TODO: Solid Leaf Surfaces
		int[] solidLeafSurfaces = new int[planes.sizeIndices()];
		for (int i = 0; i < planes.sizeIndices(); i++)
			solidLeafSurfaces[i] = i;
		result.setSolidLeafSurfaces(solidLeafSurfaces);
		
		inspector.displayProgress("Adding Animated Lights", 51);
		
		//System.out.println("Adding Animated Lights");
		result.setAnimatedLights(new AnimatedLight[0]);
		
		inspector.displayProgress("Adding Light States", 52);
		
		//System.out.println("Adding Light States");
		result.setLightStates(new LightState[0]);
		
		inspector.displayProgress("Adding Light State Data", 53);
		
		//System.out.println("Adding Light State Data");
		result.setLightStateData(new LightStateData[0]);
		
		inspector.displayProgress("Adding State Data Buffer", 54);
		
		//System.out.println("Adding State Data Buffer");
		result.setStateDataBuffer(new byte[0]);
		
		inspector.displayProgress("Adding Name Buffer", 55);
		
		//System.out.println("Adding Name Buffer");
		result.setNameBuffer(new char[0]);
		
		inspector.displayProgress("Adding Sub Objects", 56);
		
		//System.out.println("Adding Sub Objects");
		result.setSubObjects(new InteriorSubObject[0]);
		
		inspector.displayProgress("Adding Convex Hulls", 57);
		
		//System.out.println("Adding Convex Hulls");
		ConvexHull[] convexHulls = new ConvexHull[1];//vertices.sizeIndices() / 8];//surfaces.sizeIndices()];
		//for (int i = 0; i < surfaces.sizeIndices(); i++)
		//{
		//int inde = 0;
		//for (int i = 0; i < vertices.sizeIndices()-8; i+=8)
		//{
			float minX = 0;
			float maxX = 0;
			float minY = 0;
			float maxY = 0;
			float minZ = 0;
			float maxZ = 0;
			int hullStart = 0;//surfaces.getObject(i).getWindingStart();
			int surfaceStart = 0;//i;
			int planeStart = 0;//surfaces.getObject(i).getPlaneIndex();
			short hullCount = (short)vertices.sizeIndices();//(short)vertices.sizeIndices();//8;//(short)surfaces.getObject(i).getWindingCount();
			short surfaceCount = (short)surfaces.sizeIndices();//1;
			int polyListPlaneStart = 0;//i;
			int polyListPointStart = 0;//i*3;
			int polyListStringStart = 0;
			short searchTag = 0;
			boolean staticMesh = false;
			
			convexHulls[0] = new ConvexHull(minX, maxX, minY, maxY, minZ, maxZ, hullStart, surfaceStart, planeStart, hullCount, surfaceCount, polyListPlaneStart, polyListPointStart, polyListStringStart, searchTag, staticMesh);
			//inde++;
		//}
		result.setConvexHulls(convexHulls);
		
		inspector.displayProgress("Adding Convex Hull Emit Strings", 65);
		
		// TODO: Convex Hull Emit Strings
		//System.out.println("Adding Convex Hull Emit Strings");
		// To inspect this further, I want to make a test DIF with a triangular prism in it. This would give me all the properties of a triangle. I should theoretically just be able to copy paste them.
		List<Byte> convexHullByteStrings = new ArrayList<Byte>();
		/*for (int i = 0; i < convexHulls.length; i++) {
			// the number of POINTS in this triangle minus one
			convexHullByteStrings.add((byte)2);
			// the point indices
			// Torque does the work of adding the hullStart for us. We should be able to just do 0,1
			convexHullByteStrings.add((byte)0);
			convexHullByteStrings.add((byte)1);
			// regardless of whether our DIF supports edges, we need to output numEdges here. Thankfully this is always the same in a triangle.
			convexHullByteStrings.add((byte)3);
			// I don't know what to do here, other than I know they come in groups of two
			convexHullByteStrings.add((byte)0);
			convexHullByteStrings.add((byte)1);
			convexHullByteStrings.add((byte)0);
			convexHullByteStrings.add((byte)2);
			convexHullByteStrings.add((byte)0);
			convexHullByteStrings.add((byte)3);
			// Now we need to write out the number of polys which is 1, but I question whether this is good enough or not
			convexHullByteStrings.add((byte)1);
			// now we need vertex count, again questioning the legitimacy though
			convexHullByteStrings.add((byte)3);
			// now we need the planeIndex, which is by far the most straightforward thing here
			convexHullByteStrings.add((byte)convexHulls[i].getPlaneStart());
			// now we emit the vertices one after the other?
			convexHullByteStrings.add((byte)0);
			convexHullByteStrings.add((byte)1);
			convexHullByteStrings.add((byte)2);
		}*/
		byte[] convexHullEmitStrings = new byte[convexHullByteStrings.size()];
		for (int i = 0; i < convexHullByteStrings.size(); i++) {
			convexHullEmitStrings[i] = convexHullByteStrings.get(i);
		}
		result.setConvexHullEmitStrings(convexHullEmitStrings);
		
		inspector.displayProgress("Adding Hull Indices", 70);
		
		// TODO: Hull Indices
		//System.out.println("Adding Hull Indices");
		// should be * 3 not 8, there are three vertices per shape
		int[] hullIndices = new int[convexHulls.length * 3];
		for (int i = 0; i < hullIndices.length; i++)
		{
			hullIndices[i] = i;
		}
		result.setHullIndices(hullIndices);
		
		inspector.displayProgress("Adding Hull Plane Indices", 73);
		
		// TODO: Hull Plane Indices
		//System.out.println("Adding Hull Plane Indices");
		// just the planeIndexes in some order
		short[] hullPlaneIndices = new short[planes.sizeIndices()];
		for (short i = 0; i < planes.sizeIndices(); i++)
			hullPlaneIndices[i] = i;
		result.setHullPlaneIndices(hullPlaneIndices);
		// TODO: Hull Emit String Indices
		// Each triangle uses 15 numbers in the convexHullEmitStrings. This makes sense - they are all the same shape.
		
		inspector.displayProgress("Adding Hull Emit String Indices", 75);
		
		//System.out.println("Adding Hull Emit String Indices");
		int[] hullEmitStringIndices = new int[convexHulls.length];
		for (int i = 0; i < convexHulls.length; i++)
		{
			hullEmitStringIndices[i] = i*15;
		}
		result.setHullEmitStringIndices(hullEmitStringIndices);
		
		inspector.displayProgress("Adding Hull Surface Indices", 77);
		
		//System.out.println("Adding Hull Surface Indices");
		int[] hullSurfaceIndices = new int[surfaces.sizeIndices()];
		for (int i = 0; i < surfaces.sizeIndices(); i++)
			hullSurfaceIndices[i] = surfaces.indexOfObject(surfaces.getObject(i));
		result.setHullSurfaceIndices(hullSurfaceIndices);
		
		// TODO: Poly List Planes
		
		inspector.displayProgress("Adding Poly List Planes", 80);
		
		//System.out.println("Adding Poly List Planes");
		short[] polyListPlanes = new short[planes.sizeIndices()];
		for (short i = 0; i < planes.sizeIndices(); i++)
			polyListPlanes[i] = i;
		result.setPolyListPlanes(polyListPlanes);
		// TODO: Poly List Points
		
		inspector.displayProgress("Adding Poly List Points", 81);
		
		//System.out.println("Adding Poly List Points");
		int[] polyListPoints = new int[points.length];
		for (int i = 0; i < points.length; i++)
			polyListPoints[i] = i;
		result.setPolyListPoints(polyListPoints);
		// TODO: Poly List Strings
		
		inspector.displayProgress("Adding Poly List Strings", 82);
		
		//System.out.println("Adding Poly List Strings");
		List<Byte> polyByteStrings = new ArrayList<Byte>();
		/*for (int i = 0; i < convexHulls.length; i++) {
			// the number of PLANES in this convex hull, always 1 in a triangle
			polyByteStrings.add((byte)1);
			// a bunch of powers of two with one for each surface (we only have one surface - so just 1)
			// This actually becomes InterestMask, and if InterestMask AND pointString[i] is greater then zero we continue, so we want both of these to be 1 so it returns true
			polyByteStrings.add((byte)1);
			// the number 0 (for the weird vertex calculation)
			polyByteStrings.add((byte)0);
			// the number of vertices in this convex hull (always 3 in a triangle)
			polyByteStrings.add((byte)3);
			
			// these correspond to vertices in some way. They need to be one so InterestMask AND pointString[i] is true
			for (int j = 0; j < 3; j++) {
				polyByteStrings.add((byte)1);
			}
			// the number of COLLISION SURPRISES in this convex hull, always 1 in a triangle
			polyByteStrings.add((byte)1);
			// Normally we'd have to dive into a loop here and loop through the surfaces, but every convex hull we're dealing with only has one surface.
			// Number of points AGAAIIINN -_-
			polyByteStrings.add((byte)3);
			// the number of collision masks. Masks, not hoods High Speed! Remember?
			// These usually go up in powers of two but because there's only one surface in this convex hull, it's only 1.
			polyByteStrings.add((byte)1);
			// The planeIndex for the current Convex Hull (I think)
			// we can actually just use getPlaneStart since we only have one plane per convex hull
			polyByteStrings.add((byte)(convexHulls[i].getPlaneStart()));
			// we write out the indices, but only have three because there is a triangle.
			// we need to write the number zero before each because annoying.
			polyByteStrings.add((byte)0);
			// get the vertices for the CURRENT triangle, hence multiplying by 3 for every triangle here and hoping the vertices are in order
			polyByteStrings.add((byte)mesh.getIndices()[0+(i*3)]);
			polyByteStrings.add((byte)0);
			polyByteStrings.add((byte)mesh.getIndices()[1+(i*3)]);
			polyByteStrings.add((byte)0);
			polyByteStrings.add((byte)mesh.getIndices()[2+(i*3)]);
			convexHulls[i].setPolyListStringStart(i*19);
		}*/
		byte[] polyListStrings = new byte[polyByteStrings.size()];
		for (int i = 0; i < polyByteStrings.size(); i++) {
			polyListStrings[i] = polyByteStrings.get(i);
		}
		result.setPolyListStrings(polyListStrings);
		
		// TODO: CoordBins
		// TODO: CoordBinIndices
		
		inspector.displayProgress("Building CoordBins", 85);
		
		//System.out.println("Building CoordBins");
		CoordBin[] coordBins = new CoordBin[256];
		for (int i = 0; i < coordBins.length; i++)
			coordBins[i] = new CoordBin(i, 1);
		result.setCoordBins(coordBins);
		
		inspector.displayProgress("Building CoordBin Indices", 86);
		
		//System.out.println("Building CoordBin Indices");
		short[] coordBinIndices = new short[coordBins.length];
		for (short i = 0; i < coordBins.length; i++)
			coordBinIndices[i] = 0;//i;
		result.setCoordBinIndices(coordBinIndices);
		
		inspector.displayProgress("Adding CoordBinMode", 87);
		
		//System.out.println("Adding CoordBinMode");
		result.setCoordBinMode(0);
		
		inspector.displayProgress("Adding BaseAmbient", 88);
		
		//System.out.println("Adding BaseAmbient");
		result.setBaseAmbient(new ColorI((byte)0, (byte)0, (byte)0, (byte)-1));
		
		inspector.displayProgress("Adding AlarmAmbient", 89);
		
		//System.out.println("Adding AlarmAmbient");
		result.setAlarmAmbient(new ColorI((byte)0, (byte)0, (byte)0, (byte)-1));
		
		inspector.displayProgress("Adding Static Meshes", 90);
		
		//System.out.println("Adding Static Meshes");
		result.setStaticMeshes(new InteriorSimpleMesh[0]);
		
		inspector.displayProgress("Adding Normals", 91);
		
		//System.out.println("Adding Normals");
		result.setNormals(new Point3F[0]);
		
		inspector.displayProgress("Adding TexMatrices", 92);
		
		//System.out.println("Adding TexMatrices");
		result.setTexMatrices(new TexMatrix[0]);
		
		inspector.displayProgress("Adding TexMatIndices", 93);
		
		//System.out.println("Adding TexMatIndices");
		result.setTexMatIndices(new int[0]);
		
		inspector.displayProgress("Adding Light Map Border Size", 94);
		
		//System.out.println("Adding Light Map Border Size");
		result.setLightMapBorderSize(2);
		
		// TODO: BSPNodes
		
		inspector.displayProgress("Adding BSPNodes", 95);
		
		//System.out.println("Adding BSPNodes");
		BSPNode[] bspNodes = new BSPNode[surfaces.sizeIndices()];
		for (int i = 0; i < surfaces.sizeIndices(); i++)
		{
			// BSPNodes may be incorrect
			// Consider revisiting
			//short oppositeIndex = (short)(surfaces.sizeIndices() - i);
			short planeIndex = (short)i;
			int backIndex = -16384 + i;
			bspNodes[i] = new BSPNode(planeIndex, -32768, backIndex);
		}
		result.setBspNodes(bspNodes);
		
		inspector.displayProgress("Adding BSPSolidLeaves", 96);
		
		//System.out.println("Adding BSPSolidLeaves");
		BSPSolidLeaf[] bspSolidLeaves = new BSPSolidLeaf[surfaces.sizeIndices()];
		for (int i = 0; i < surfaces.sizeIndices(); i++)
			bspSolidLeaves[i] = new BSPSolidLeaf(i, (short)1);
		result.setBspSolidLeaves(bspSolidLeaves);
		
		inspector.displayProgress("Processing Data", 97);
		
		//System.out.println("Processing Data");
		result.processData();
		
		inspector.displayProgress("Preparing Data", 98);
		
		//System.out.println("Preparing Data");
		result.prepareData();
		
		inspector.displayProgress("Recalculating Bounds", 99);
		
		//System.out.println("Recalculating Bounds");
		//result.recalculateBounds();
		
		result.setMinPixels(250);
		
		inspector.displayProgress(100);
		inspector.displayProgress(false);
		
		return result;
	}
	
}
