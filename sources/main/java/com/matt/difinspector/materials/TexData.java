package com.matt.difinspector.materials;

import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;

public class TexData
{
	private Point3F normal;
	private float rotate;
	private Point2F offset;
	private Point2F scale;
	
	public TexData(Point3F normal, Point2F offset, float rotate, Point2F scale)
	{
		this.normal = normal;
		this.offset = offset;
		this.rotate = rotate;
		this.scale = scale;
	}

	public Point3F getNormal()
	{
		return normal;
	}

	public void setNormal(Point3F normal)
	{
		this.normal = normal;
	}

	public float getRotate()
	{
		return rotate;
	}

	public void setRotate(float rotate)
	{
		this.rotate = rotate;
	}

	public Point2F getOffset()
	{
		return offset;
	}

	public void setOffset(Point2F offset)
	{
		this.offset = offset;
	}

	public Point2F getScale()
	{
		return scale;
	}

	public void setScale(Point2F scale)
	{
		this.scale = scale;
	}
}
