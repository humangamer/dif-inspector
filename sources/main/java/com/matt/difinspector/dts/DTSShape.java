package com.matt.difinspector.dts;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.matt.difinspector.io.ArrayReader;
import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.structures.TSMaterialList;

public class DTSShape
{
	protected int fileVersion;
	protected int exporterVersion;
	protected Sequence[] sequences;
	protected TSMaterialList materialList;
	protected float smallestVisibleSize;
	protected int smallestVisibleDL;
	
	public DTSShape()
	{
		
	}
	
	public boolean read(File file)
	{
		try {
			
			FileInputStream fis = new FileInputStream(file);
			ReverseDataInputStream dis = new ReverseDataInputStream(new BufferedInputStream(fis));
			
			this.fileVersion = dis.readInt();
			this.exporterVersion = this.fileVersion >> 16;
			this.fileVersion &= 0xFF;
			if (this.fileVersion > 24)
			{
				System.err.println("Incompatible DTS File Version: " + this.fileVersion);
				dis.close();
				return false;
			}
			
			int[] memBuffer32;
			short[] memBuffer16;
			byte[] memBuffer8;
			int count32, count16, count8;
			if (this.fileVersion < 19)
			{
				System.err.println("... Shape with old version.");
				dis.close();
				return false;
			} else {
				int sizeMemBuffer = dis.readInt();
				int start16 = dis.readInt();
				int start8 = dis.readInt();
				
				byte[] tmp = new byte[4*sizeMemBuffer];
				dis.read(tmp, 0, tmp.length);
				
				count32 = start16;
				count16 = start8 - start16;
				count8 = sizeMemBuffer - start8;
				
				memBuffer32 = new int[count32];
				int index = 0;
				for (int i = 0; i < memBuffer32.length; i++)
				{
					byte ch1 = tmp[index+0];
					byte ch2 = tmp[index+1];
					byte ch3 = tmp[index+2];
					byte ch4 = tmp[index+3];
					memBuffer32[i] = ((ch4 << 24) + (ch3 << 16) + (ch2 << 8) + (ch1 << 0));
					index += 4;
				}
				
				memBuffer16 = new short[count16];
				index = 0;
				for (int i = start16; i < memBuffer16.length; i++)
				{
					byte ch1 = tmp[index+0];
					byte ch2 = tmp[index+1];
					memBuffer16[i] = (short)((ch2 << 8) + (ch1 << 0));
					index += 2;
				}
				
				memBuffer8 = new byte[count8];
				for (int i = start8; i < memBuffer8.length; i++)
				{
					memBuffer8[i] = tmp[i];
				}
				
				int numSequences = dis.readInt();
				this.sequences = new Sequence[numSequences];
				for (int i = 0; i < numSequences; i++)
				{
					this.sequences[i] = new Sequence();
					if (!this.sequences[i].read(dis, this.fileVersion, true))
						return false;
				}
				
				this.materialList = new TSMaterialList(this.fileVersion);
				if (!this.materialList.read(dis))
					return false;
			}
			
			ArrayReader reader = new ArrayReader(memBuffer32, memBuffer16, memBuffer8);
			this.assembleShape(reader);
			
			dis.close();
			
			return true;
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void assembleShape(ArrayReader ar) throws IOException
	{
		/*int numNodes = ar.readInt();
		int numObjects = ar.readInt();
		int numDecals = ar.readInt();
		int numSubShapes = ar.readInt();
		int numIflMaterials = ar.readInt();
		int numNodeRots;
		int numNodeTrans;
		int numNodeUniformScales;
		int numNodeAlignedScales;
		int numNodeArbitraryScales;
		if (this.fileVersion < 22)
		{
			numNodeRots = numNodeTrans = ar.readInt() - numNodes;
			numNodeUniformScales = numNodeAlignedScales = numNodeArbitraryScales = 0;
		} else {
			numNodeRots = ar.readInt();
			numNodeTrans = ar.readInt();
			numNodeUniformScales = ar.readInt();
			numNodeAlignedScales = ar.readInt();
			numNodeArbitraryScales = ar.readInt();
		}
		int numGroundFrames = 0;
		if (this.fileVersion > 23)
		{
			numGroundFrames = ar.readInt();
		}
		int numObjectStates = ar.readInt();
		int numDecalStates = ar.readInt();
		int numTriggers = ar.readInt();
		int numDetails = ar.readInt();
		int numMeshes = ar.readInt();
		int numSkins = 0;
		if (this.fileVersion < 23)
		{
			numSkins = ar.readInt();
		}
		int numNames = ar.readInt();
		this.smallestVisibleSize = (float)ar.readInt();
		this.smallestVisibleDL = ar.readInt();
		int skipDL = Math.min(this.smallestVisibleDL, 0);

		System.out.println("Num Nodes: " + numNodes);
		System.out.println("Num Objects: " + numObjects);
		System.out.println("Num Decals: " + numDecals);
		System.out.println("Num Sub Shapes: " + numSubShapes);
		System.out.println("Num Ifl Materials: " + numIflMaterials);*/
	}
	
	public void print()
	{
		System.out.println("File Version: " + this.fileVersion);
		System.out.println("Exporter Version: " + this.exporterVersion);
		
		System.out.println("Sequences: " + this.sequences.length);
		for (int i = 0; i < this.sequences.length; i++)
		{
			System.out.print("Sequence[" + i + "]: ");
			this.sequences[i].print();
		}
		System.out.println("Materials: " + this.materialList.getMaterials().size());
		for (int i = 0; i < this.materialList.getMaterials().size(); i++)
		{
			System.out.println("Material[" + i + "]: " + this.materialList.getMaterials().get(i).getName());
		}
	}
}
