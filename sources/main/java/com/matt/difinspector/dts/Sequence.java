package com.matt.difinspector.dts;

import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;

public class Sequence
{
	protected int nameIndex;
	protected int numKeyFrames;
	protected float duration;
	protected int baseRotation;
	protected int baseTranslation;
	protected int baseScale;
	protected int baseObjectState;
	protected int baseDecalState;
	protected int firstGroundFrame;
	protected int numGroundFrames;
	protected int firstTrigger;
	protected int numTriggers;
	protected float toolBegin;
	
	protected int[] rotationMatters;
	protected int[] translationMatters;
	protected int[] scaleMatters;
	protected int[] visMatters;
	protected int[] frameMatters;
	protected int[] matFrameMatters;
	protected int[] decalMatters;
	protected int[] iflMatters;
	
	protected int priority;
	protected int flags;
	protected int dirtyFlags;
	
	public Sequence()
	{
		
	}
	
	public boolean read(ReverseDataInputStream dis, int fileVersion, boolean readNameIndex) throws IOException
	{
		if (readNameIndex)
			this.nameIndex = dis.readInt();
		
		this.flags = 0;
		
		if (fileVersion > 21)
			this.flags = dis.readInt();
		
		this.numKeyFrames = dis.readInt();
		this.duration = dis.readFloat();
		
		if (fileVersion < 22)
		{
			boolean tmp = dis.readBoolean();
			if (tmp)
				flags |= ShapeFlags.Blend.getValue();
			tmp = dis.readBoolean();
			if (tmp)
				flags |= ShapeFlags.Cyclic.getValue();
			tmp = dis.readBoolean();
			if (tmp)
				flags |= ShapeFlags.MakePath.getValue();
		}
		
		this.priority = dis.readInt();
		this.firstGroundFrame = dis.readInt();
		this.numGroundFrames = dis.readInt();
		
		if (fileVersion > 21)
		{
			this.baseRotation = dis.readInt();
			this.baseTranslation = dis.readInt();
			this.baseScale = dis.readInt();
			this.baseObjectState = dis.readInt();
			this.baseDecalState = dis.readInt();
		} else {
			this.baseRotation = dis.readInt();
			this.baseTranslation = this.baseRotation;
			this.baseObjectState = dis.readInt();
			this.baseDecalState = dis.readInt();
		}
		
		this.firstTrigger = dis.readInt();
		this.numTriggers = dis.readInt();
		this.toolBegin = dis.readFloat();
		
		// this.rotationMatters.read(dis);
		
		if (fileVersion < 22)
		{
			this.translationMatters = this.rotationMatters;
		} else {
			// this.translationMatters.read(dis);
			// this.scaleMatters.read(dis);
		}
		
		// this.decalMatters.read(dis);
		// this.iflMatters.read(dis);
		// this.visMatters.read(dis);
		// this.frameMatters.read(dis);
		// this.matFrameMatters.read(dis);
		
		this.dirtyFlags = 0;
		/*if (rotationMatters.testAll() || translationMatters.testAll() || scaleMatters.testAll())
			dirtyFlags |= TransformDirty;
		if (visMatters.testAll())
			dirtyFlags |= VisDirty;
		if (frameMatters.testAll())
			dirtyFlags |= FrameDirty;
		if (matFrameMatters.testAll())
			dirtyFlags |= MatFrameDirty;
		if (decalMatters.testAll())
			dirtyFlags |= DecalDirty;
		if (iflMatters.testAll())
			dirtyFlags |= IflDirty;*/
		
		return true;
	}
	
	public boolean write(ReverseDataOutputStream dos) throws IOException
	{
		return false;
	}
	
	public void print()
	{
		System.out.println("Name Index: " + this.nameIndex);
		System.out.println("Num Key Frames: " + this.numKeyFrames);
		System.out.println("Duration: " + this.duration);
		System.out.println("Base Rotation: " + this.baseRotation);
		System.out.println("Base Translation: " + this.baseTranslation);
		System.out.println("Base Scale: " + this.baseScale);
		System.out.println("Base Object State: " + this.baseObjectState);
		System.out.println("Base Decal State: " + this.baseDecalState);
		System.out.println("First Ground Frame: " + this.firstGroundFrame);
		System.out.println("Num Ground Frames: " + this.numGroundFrames);
		System.out.println("First Trigger: " + this.firstTrigger);
		System.out.println("Num Triggers: " + this.numTriggers);
		System.out.println("Tool Begin: " + this.toolBegin);
		System.out.println();
		System.out.println("Priority: " + this.priority);
		System.out.println("Flags: " + this.flags);
		System.out.println("Dirty Flags: " + this.dirtyFlags);
	}
}
