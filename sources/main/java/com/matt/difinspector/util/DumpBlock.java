package com.matt.difinspector.util;

import java.util.ArrayList;
import java.util.List;

public class DumpBlock
{
	protected List<DumpBlock> children;
	protected String value;
	
	public DumpBlock()
	{
		this.children = new ArrayList<DumpBlock>();
	}
	
	public DumpBlock(String value)
	{
		this();
		this.value = value;
	}
	
	public DumpBlock(Object[] array)
	{
		this();
		
		this.add(new DumpBlock("length = " + array.length + ";"));
		for (int i = 0; i < array.length; i++)
		{
			this.add(new DumpBlock(array[i].toString()));
		}
	}
	
	public DumpBlock(String name, String value)
	{
		this("DumpBlock: " + name);
		this.add(new DumpBlock(value));
	}
	
	public DumpBlock(String name, Object[] value)
	{
		this("DumpBlock: " + name);
		this.add(new DumpBlock(value));
	}
	
	public void add(DumpBlock block)
	{
		this.children.add(block);
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		if (this.value == null)
			this.value = "<NULL>";
		sb.append(this.value);
		
		sb.append("{\n");
		for (int i = 0; i < this.children.size(); i++)
		{
			sb.append(this.children.get(i));
		}
		sb.append("}");
		
		return sb.toString();
	}
}
