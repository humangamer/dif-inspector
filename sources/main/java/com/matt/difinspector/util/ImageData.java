package com.matt.difinspector.util;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class ImageData
{
	private BufferedImage image;
	
	private ImageData(BufferedImage image)
	{
		this.image = image;
	}
	
	public static final ImageData loadImage(String path)
	{
		if (path.endsWith("_mbm"))
			path = path.substring(0, path.lastIndexOf("_mbm"));
		BufferedImage img = null;
		
		try {
			InputStream is = ImageData.class.getResourceAsStream("/" + path + ".png");
			if (is == null)
			{
				is = ImageData.class.getResourceAsStream("/" + path + ".jpg");
				if (is == null)
				{
					System.out.println("Unable to load image: " + path);
					return null;
				}
			}
			
			img = ImageIO.read(is);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		if (img == null)
			return null;
		
		return new ImageData(img);
	}
	
	public int getWidth()
	{
		return this.image.getWidth();
	}
	
	public int getHeight()
	{
		return this.image.getHeight();
	}
	
	public BufferedImage getImage()
	{
		return this.image;
	}
}
