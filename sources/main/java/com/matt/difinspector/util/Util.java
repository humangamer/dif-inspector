package com.matt.difinspector.util;

import static org.lwjgl.opengl.GL11.*;

import java.io.File;
import java.nio.IntBuffer;
import java.util.List;
import java.util.Vector;

import org.lwjgl.BufferUtils;

import com.matt.difinspector.materials.TexData;
import com.matt.difinspector.materials.TorqueMaterial;
import com.matt.difinspector.math.MatrixF;
import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.TexGenPlanes;

public final class Util
{
	public static final int BIT(int bit)
	{
		return 1 << bit;
	}
	
	public static final short convertInt(int num)
	{
		//if (num == 0x1000)
		//	return (short)num;
		short result = (short) (((num & 0xF0000) >> 4) + (num & 0xFFF));
		if (result == 0)
			return (short)num;
		
		return result;
	}
	
	public static final int arrayMatch(int[] array, int needle) {
		int matches = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == needle) {
				matches++;
			}
		}
		
		return matches;
	}
	
	public static final int arrayMatch(List<Integer> list, int needle) {
		int matches = 0;
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) == needle) {
				matches++;
			}
		}
		
		return matches;
	}
	
	public static final boolean isPow2(int num)
	{
		return (num & (num - 1)) == 0;
	}
	
	public static final int getBinLog2(int num)
	{
		return Util.getBinLog2(num, false);
	}
	
	public static int log(int x, int base)
	{
	    return (int) (Math.log(x) / Math.log(base));
	}
	
	public static final int getBinLog2(int num, boolean knownPow2)
	{
		int[] MultiplyDeBruijnBitPosition = new int[] { 0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8, 31,
				27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9 };
				
		if (!knownPow2)
		{
			num |= num >> 1; // first round down to power of 2
			num |= num >> 2;
			num |= num >> 4;
			num |= num >> 8;
			num |= num >> 16;
			num = (num >> 1) + 1;
		}
		
		return MultiplyDeBruijnBitPosition[(num * 0x077CB531) >> 27];
	}
	
	public static final Vector<Vector<String>> toStringVector(String... strings)
	{
		Vector<Vector<String>> res = new Vector<Vector<String>>();
		for (String s : strings)
		{
			Vector<String> v = new Vector<String>();
			v.addElement(s);
			res.addElement(v);
		}
		return res;
	}
	
	public static final Vector<Vector<String>> toStringVector(List<String> list)
	{
		Vector<Vector<String>> res = new Vector<Vector<String>>();
		for (String s : list)
		{
			Vector<String> v = new Vector<String>();
			v.addElement(s);
			res.addElement(v);
		}
		return res;
	}
	
	public static final Vector<Vector<String>> stringListToStringVector(List<List<String>> list)
	{
		Vector<Vector<String>> res = new Vector<Vector<String>>();
		for (int i = 0; i < list.size(); i++)
		{
			List<String> l = list.get(i);
			Vector<String> v = new Vector<String>();
			v.addElement("" + i);
			for (int j = 0; j < l.size(); j++)
			{
				String s = l.get(j);
				v.addElement(s);
			}
			res.addElement(v);
		}
		return res;
	}
	
	public static final Vector<Vector<String>> materialToStringVector(List<TorqueMaterial> list)
	{
		Vector<Vector<String>> res = new Vector<Vector<String>>();
		for (int i = 0; i < list.size(); i++)
		{
			String s = list.get(i).getName();
			Vector<String> v = new Vector<String>();
			v.addElement("" + i);
			v.addElement(s);
			res.addElement(v);
		}
		return res;
	}
	
	public static final String getArrayString(byte[] arr)
	{
		if (arr == null)
			return "<NULL>";
		StringBuilder sb = new StringBuilder();
		sb.append("byte[" + arr.length + "]{");
		for (int i = 0; i < arr.length; i++)
		{
			sb.append(arr[i]);
			if (i < arr.length - 1)
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static final String getArrayString(short[] arr)
	{
		if (arr == null)
			return "<NULL>";
		StringBuilder sb = new StringBuilder();
		sb.append("short[" + arr.length + "]{");
		for (int i = 0; i < arr.length; i++)
		{
			sb.append(arr[i]);
			if (i < arr.length - 1)
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static final String getArrayString(int[] arr)
	{
		if (arr == null)
			return "<NULL>";
		StringBuilder sb = new StringBuilder();
		sb.append("int[" + arr.length + "]{");
		for (int i = 0; i < arr.length; i++)
		{
			sb.append(arr[i]);
			if (i < arr.length - 1)
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static final String getArrayString(long[] arr)
	{
		if (arr == null)
			return "<NULL>";
		StringBuilder sb = new StringBuilder();
		sb.append("long[" + arr.length + "]{");
		for (int i = 0; i < arr.length; i++)
		{
			sb.append(arr[i]);
			if (i < arr.length - 1)
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static final String getArrayString(float[] arr)
	{
		if (arr == null)
			return "<NULL>";
		StringBuilder sb = new StringBuilder();
		sb.append("float[" + arr.length + "]{");
		for (int i = 0; i < arr.length; i++)
		{
			sb.append(arr[i]);
			if (i < arr.length - 1)
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static final String getArrayString(double[] arr)
	{
		if (arr == null)
			return "<NULL>";
		StringBuilder sb = new StringBuilder();
		sb.append("double[" + arr.length + "]{");
		for (int i = 0; i < arr.length; i++)
		{
			sb.append(arr[i]);
			if (i < arr.length - 1)
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static final String getArrayString(boolean[] arr)
	{
		if (arr == null)
			return "<NULL>";
		StringBuilder sb = new StringBuilder();
		sb.append("boolean[" + arr.length + "]{");
		for (int i = 0; i < arr.length; i++)
		{
			sb.append((arr[i] ? "true" : "false"));
			if (i < arr.length - 1)
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static final String getArrayString(char[] arr)
	{
		if (arr == null)
			return "<NULL>";
		StringBuilder sb = new StringBuilder();
		sb.append("char[" + arr.length + "]{");
		for (int i = 0; i < arr.length; i++)
		{
			sb.append(arr[i]);
			if (i < arr.length - 1)
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static final String getArrayString(Object[] arr)
	{
		if (arr == null)
			return "<NULL>";
		StringBuilder sb = new StringBuilder();
		sb.append("Object[" + arr.length + "]{");
		for (int i = 0; i < arr.length; i++)
		{
			sb.append(arr[i]);
			if (i < arr.length - 1)
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static final String getArrayString(List<?> arr)
	{
		if (arr == null)
			return "<NULL>";
		StringBuilder sb = new StringBuilder();
		sb.append("byte[" + arr.size() + "]{");
		for (int i = 0; i < arr.size(); i++)
		{
			sb.append(arr.get(i));
			if (i < arr.size() - 1)
				sb.append(", ");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static final int[] getIntArray(Integer[] arr)
	{
		if (arr == null)
			return null;
		int[] result = new int[arr.length];
		for (int i = 0; i < arr.length; i++)
			result[i] = arr[i];
		return result;
	}
	
	public static final int countElements(Object[] arr)
	{
		return arr == null ? 0 : arr.length;
	}
	
	public static final int countElements(List<?> arr)
	{
		return arr == null ? 0 : arr.size();
	}
	
	public static final boolean hasElements(Object[] arr)
	{
		return Util.countElements(arr) > 0;
	}
	
	public static final boolean hasElements(List<?> arr)
	{
		return Util.countElements(arr) > 0;
	}
	
	private static float[][] baseAxis = new float[][]
		{
		   {0,0,1}, {1,0,0}, {0,-1,0},			// floor
		   {0,0,-1}, {1,0,0}, {0,-1,0},		// ceiling
		   {1,0,0}, {0,1,0}, {0,0,-1},			// west wall
		   {-1,0,0}, {0,1,0}, {0,0,-1},		// east wall
		   {0,1,0}, {1,0,0}, {0,0,-1},			// south wall
		   {0,-1,0}, {1,0,0}, {0,0,-1}			// north wall
		};
	
	public static float[][] getTextureAxis(Point3F normal)
	{
		int bestAxis = 0;
		float best = 0;
		float dot;
		
		for (int i = 0; i < 6; i++)
		{
			dot = normal.dot(new Point3F(Util.baseAxis[i*3][0], Util.baseAxis[i*3][1], Util.baseAxis[i*3][2]));
			if (dot > best)
			{
				best = dot;
				bestAxis = i;
			}
		}
		
		float[][] result = new float[2][3];
		result[0][0] = Util.baseAxis[bestAxis * 3 + 1][0];
		result[0][1] = Util.baseAxis[bestAxis * 3 + 1][1];
		result[0][2] = Util.baseAxis[bestAxis * 3 + 1][2];
		result[1][0] = Util.baseAxis[bestAxis * 3 + 2][0];
		result[1][1] = Util.baseAxis[bestAxis * 3 + 2][1];
		result[1][2] = Util.baseAxis[bestAxis * 3 + 2][2];
		
		return result;
	}
	
	public static TexGenPlanes getTexGen(TexData texData, ImageData texture)
	{
		float width = texture.getWidth();
		float height = texture.getHeight();
		
		TexGenPlanes planes = new TexGenPlanes(texData.getNormal(), texData.getOffset(), texData.getRotate(), texData.getScale());
		
		planes.scaleDown(width, height);
		
		planes.scaleUp(32);
		
		return planes;
	}
	
	public static TexData getTexData(Point3F normal, TexGenPlanes planes, ImageData texture)
	{
		float width = texture.getWidth();
		float height = texture.getHeight();
		
		PlaneF planeX = new PlaneF(planes.getPlaneX().getX(), planes.getPlaneX().getY(), planes.getPlaneX().getZ(), planes.getPlaneX().getD());
		PlaneF planeY = new PlaneF(planes.getPlaneY().getX(), planes.getPlaneY().getY(), planes.getPlaneY().getZ(), planes.getPlaneY().getD());
		
		planeX.scaleDown(32);
		planeY.scaleDown(32);
		
		planeX.scaleUp(width);
		planeY.scaleUp(height);
		
		float[][] vecs = Util.getTextureAxis(normal);
		
		float[][] v = new float[2][3];
		
		// These are scaled by xScale
		v[0][0] = planeX.getX();
		v[0][1] = planeX.getY();
		v[0][2] = planeX.getZ();
		
		//System.out.println("X: " + planeX);
		//System.out.println("Y: " + planeY);
		
		v[1][0] = planeY.getX();
		v[1][1] = planeY.getY();
		v[1][2] = planeY.getZ();
		
		//float xScale = v[0][0];
		//float yScale = v[1][0];
		
		int sv;
		int tv;
		
		//float ns = 0;
		//float nt = 0;
		
		if (vecs[0][0] != 0.0f)
		{
			sv = 0;
		} else if (vecs[0][1] != 0.0f)
		{
			sv = 1;
		} else {
			sv = 2;
		}
		
		if (vecs[1][0] != 0.0f)
		{
			tv = 0;
		} else if (vecs[1][1] != 0.0f)
		{
			tv = 1;
		} else {
			tv = 2;
		}
		
		//float sinv;
		//float cosv;
		float angle = 0;
		
		//float nsM = 0;
		//float ntM = 0;
		
		for (int i = 0; i < 2; i++)
		{
			//ns = vecs[i][sv];
			//nt = vecs[i][tv];
			
			//nsM = v[i][sv];
			//ntM = v[i][tv];
			
			//System.out.println("[" + i + "]: " + nsM + ", " + ntM);
			
			//float x = ns;
			//float y = ns;
			//float z = ntM;
			
			//float sq = -(x*x) + y*y + z*z;
			//float angle2 = (float) (2 * Math.atan(-z + (Math.sqrt(sq)) / (x+y)));
			
			//if (!Float.isNaN(angle2))
			//	angle = angle2;
			
			//angle = angle1 / angle2;
			//System.out.println(angle1 + ", " + angle2 + " = " + angle);
			//System.out.println(angle2);
		
		//ns = vecs[0][sv];
		//nt = vecs[0][tv];
		
		// x = cos(a) * y - sin(a) * z
		// x = vecs[i][sv] = ns
		// y = vecs[i][sv] = ns
		// z = vecs[i][tv] = nt

		//a = 2*tan^(-1)((-z+sqrt(-x^2+y^2+z^2))/(x+y))
		
		//a = 2*tan^(-1)((-z+sqrt(-x^2+y^2+z^2))/(x+y))
		
		//a = 2*tan^(-1)((-nt+sqrt(-ns^2+ns^2+nt^2))/(ns+ns))
		
		//double a = -Math.pow(ns, 2);
		//double b = Math.pow(ns, 2);
		//double c = Math.pow(nt, 2);
		//double sq = Math.sqrt(a+b+c);
		
		//double p1 = sq - nt;
		//double p2 = ns + ns;
		//float angle = (float) (2.0f * Math.atan(p1 / p2));
		//angle = (float) (2 * Math.atan((Math.sqrt(Math.pow(-ns, 2) + Math.pow(ns, 2) + Math.pow(nt, 2)) - nt) / (ns+ns)));
		
		//System.out.println(angle);
			
			// cosv * vecs[i][sv] - sinv * vecs[i][tv] = vecs[i][sv]
			// cosv * vecs[i][sv] = vecs[i][sv] + sinv * vecs[i][tv]
			
			// cos(a) * vecs[i][sv] = vecs[i][sv] + sin(a) * vecs[i][tv]
			// x = 5
			// y = 3
			// cos(a) * x = x + sin(a) * y
			// cos(a) * 5 = 5 + sin(a) * 3
			// cos(a) = 5 + sin(a) * 3 / 5
			// cos(a) = x + sin(a) * y / x
			// cos(a) = 5 + sin(a) * 3 / 5
			
			//ns = cosv * vecs[i][sv] - sinv * vecs[i][tv];
			//nt = sinv * vecs[i][sv] + cosv * vecs[i][tv];
		}
		
		//float angle = Math.asin(a)
		
		//float angle = (float) (rotate / 180 * Math.PI);
		//float sinv = (float) Math.sin(angle);
		//float cosv = (float) Math.cos(angle);
		
		// 4      / 5      * 10    = 8
		// planeX / scaleX * width = planeXNew
		// 1 / (8 / 10 / 4) = 5
		// 1 / (planeXNew / width / planeX) = scaleX 
		//float xScale = 1 / (v[0][0] + v[0][1] + v[0][2]) / (vecs[0][0] + vecs[0][1] + vecs[0][2]);// 1 / (planeX.getX() + planeX.getY() + planeX.getZ());//1 / (planeX.getX() / width);
		//float yScale = 1 / (v[1][0] + v[1][1] + v[1][2]) / (vecs[1][0] + vecs[1][1] + vecs[1][2]);//1 / (planeY.getX() + planeY.getY() + planeY.getZ());//1 / (planeY.getX() / height);
		
		float xs;
		float ys;
		
		/*if (v[0][sv] + v[0][tv] < 0.5f)
		{
			xs = (float) Math.floor(v[0][sv] + v[0][tv]);
		} else {
			xs = (float) Math.ceil(v[0][sv] + v[0][tv]);
		}
		
		if (v[1][sv] + v[1][tv] < 0.5f)
		{
			ys = (float) Math.floor(v[1][sv] + v[1][tv]);
		} else {
			ys = (float) Math.ceil(v[1][sv] + v[1][tv]);
		}*/
		
		xs = Math.round(v[0][sv] + v[0][tv]);
		ys = Math.round(v[1][sv] + v[1][tv]);
		
		//System.out.println("K: " + xs + ", " + ys);
		
		float xScale = 1 / xs;
		float yScale = 1 / ys;
		
		Point2F offset = new Point2F(planeX.getD(), planeY.getD());
		float rotate = (float) Math.toDegrees(angle);
		Point2F scale = new Point2F(xScale, yScale);
		
		return new TexData(normal, offset, rotate, scale);
	}
	
	/*public static Point3F solveSystem(float a, float b, float c, float d, float e, float f, float g, float h, float i, float j, float k, float l)
	{
		float zTop = (l - (i * d / a)) - (((j - (i * b / a)) * (h - (e * d / a))) / (f - (e * b / a)));
		float zBot = (((j - (i * b / a )) * ((e * c / a) - g)) / (f - (e * b / a))) + (k - (i * c / a));
		float z = zTop / zBot;
		
		float y = ((h - (e * d / a)) + (((e * c / a) - g) * z)) / (f - (e * b / a));
		float x = (d - (b * y) - (c * z)) / a;
		
		return new Point3F(x, y, z);
	}*/
	
	public static boolean closeEnough(float p0, float p1, float distance)
	{
		return Math.abs(p0 - p1) < distance;
	}
	
	public static boolean closeEnough(float p0, float p1)
	{
		return closeEnough(p0, p1, 0.0001f);
	}
	
	public static Point3F solveMatrix(MatrixF pointMatrix)
	{
		MatrixF m = pointMatrix.copy();
		
		//Clean up stuff that is almost zero
		if (closeEnough(m.get(0, 0), 0.0f)) m.set(0, 0, 0.0f);
		if (closeEnough(m.get(1, 0), 0.0f)) m.set(1, 0, 0.0f);
		if (closeEnough(m.get(2, 0), 0.0f)) m.set(2, 0, 0.0f);
		if (closeEnough(m.get(0, 1), 0.0f)) m.set(0, 1, 0.0f);
		if (closeEnough(m.get(1, 1), 0.0f)) m.set(1, 1, 0.0f);
		if (closeEnough(m.get(2, 1), 0.0f)) m.set(2, 1, 0.0f);
		if (closeEnough(m.get(0, 2), 0.0f)) m.set(0, 2, 0.0f);
		if (closeEnough(m.get(1, 2), 0.0f)) m.set(1, 2, 0.0f);
		if (closeEnough(m.get(2, 2), 0.0f)) m.set(2, 2, 0.0f);
		if (closeEnough(m.get(0, 3), 0.0f)) m.set(0, 3, 0.0f);
		if (closeEnough(m.get(1, 3), 0.0f)) m.set(1, 3, 0.0f);
		if (closeEnough(m.get(2, 3), 0.0f)) m.set(2, 3, 0.0f);

		//For checking at the end
		MatrixF test = m.copy();
		
		/*
		 We have three simultaneous equations:
		 ax + by + cz = uv0
		 dx + ey + fz = uv1
		 gx + hy + iz = uv2

		 We can arrange them in a matrix like this:
		 [ a b c ]   ( x )   ( uv0 )
		 [ d e f ] x ( y ) = ( uv1 )
		 [ g h i ]   ( z )   ( uv2 )
		 
		 And then if we can get the matrix into reduced echelon form we can solve
		 for (x y z) with no trouble.
		 */

		//Swap around rows so that we can get something non-zero for [0][0]
		if (closeEnough(m.get(0, 0), 0.0f))
		{
			if (closeEnough(m.get(1, 0), 0.0f))
			{
				m.swapRows(0, 2);
			} else {
				m.swapRows(0, 1);
			}
		}
		
		//If all three have zero for [0][0] then we're fine here
		if (!closeEnough(m.get(0, 0), 0.0f))
		{
			/*
			 Reduce second and third rows so the first column is zero
			 To get
			 [ a b c ]
			 [ 0 d e ]
			 [ 0 f g ]
			 */
			if (!closeEnough(m.get(1, 0), 0.0f))
			{
				m.addRow(1, 0, -m.get(1, 0) / m.get(0, 0));
			}
			if (!closeEnough(m.get(2, 0), 0.0f))
			{
				m.addRow(2, 0, -m.get(2, 0) / m.get(0, 0));
			}
		}

		//If mat[1][1] is zero we should swap rows 1 and 2 so we can reduce the other row
		if (closeEnough(m.get(1, 1), 0.0f))
		{
			m.swapRows(1, 2);
		}
		//If mat[1][1] is zero then both [1][1] and [2][1] are zero and we can continue
		if (!closeEnough(m.get(1, 1), 0.0f))
		{
			/*
			 Reverse third row so the second column is zero
			 To get
			 [ a b c ]
			 [ 0 d e ]
			 [ 0 0 f ]
			 */
			if (!closeEnough(m.get(2, 1), 0.0f))
			{
				m.addRow(2, 1, -m.get(2, 1) / m.get(1, 1));
			}
		}

		/*
		 Scale each of the rows so the first component is one
		 To get
		 [ 1 a b ]
		 [ 0 1 c ]
		 [ 0 0 1 ]
		 */
		if (!closeEnough(m.get(0, 0), 0.0f, 0.00001f))
		{
			m.scaleRow(0, 1.0f / m.get(0, 0));
		}
		if (!closeEnough(m.get(1, 1), 0.0f, 0.00001f))
		{
			m.scaleRow(1, 1.0f / m.get(1, 1));
		}
		if (!closeEnough(m.get(2, 2), 0.0f, 0.00001f))
		{
			m.scaleRow(2, 1.0f / m.get(2, 2));
		}
		
		/*
		 At this point the matrix is

		 [ 1 a b ]   ( x )   ( uv0' )
		 [ 0 1 c ] x ( y ) = ( uv1' )
		 [ 0 0 1 ]   ( z )   ( uv2' )

		 where
		 x + ay + bz = uv0
		      y + cz = uv1
		           z = uv2

		 therefore
		 z = uv2'
		 y = uv1' - cz
		 x = uv0' - ay - bz

		 */

		//Convenience
		float[] xvec = m.getM()[0];
		float[] yvec = m.getM()[1];
		float[] zvec = m.getM()[2];
		
		//These are easy now
		float z = zvec[3];
		float y = yvec[3] - z * yvec[2];
		float x = xvec[3] - y * xvec[1] - z * xvec[2];
		
		//Check our work
		if (!closeEnough(x * test.get(0, 0) + y * test.get(0, 1) + z * test.get(0, 2), test.get(0, 3)))
		{
			System.err.println("Invalid X: (" + x + ", " + y + ", " + z + ")");
		}
		if (!closeEnough(x * test.get(1, 0) + y * test.get(1, 1) + z * test.get(1, 2), test.get(1, 3)))
		{
			System.err.println("Invalid Y: (" + x + ", " + y + ", " + z + ")");
		}
		if (!closeEnough(x * test.get(2, 0) + y * test.get(2, 1) + z * test.get(2, 2), test.get(2, 3)))
		{
			System.err.println("Invalid Z: (" + x + ", " + y + ", " + z + ") :: " + (x * test.get(2, 0) + y * test.get(2, 1) + z * test.get(2, 2)) + " :: " + test.get(2, 3));
		}

		//And there we go
		return new Point3F(x, y, z);
	}
	
	// Thanks to HiGuy
	public static TexGenPlanes getTexGen(Point3F point0, Point3F point1, Point3F point2, Point2F uv0, Point2F uv1, Point2F uv2)
	{
		TexGenPlanes planes = new TexGenPlanes();

		//Construct these matrices for the solver to figure out
		MatrixF xTexMat = new MatrixF(new float[][] {
			new float[] {
					point0.getX(), point0.getY(), point0.getZ(), uv0.getX()
			},
			new float[] {
					point1.getX(), point1.getY(), point1.getZ(), uv1.getX()
			},
			new float[] {
					point2.getX(), point2.getY(), point2.getZ(), uv2.getX()
			},
			new float[] {
					0.0f, 0.0f, 0.0f, 0.0f
			}
		});
		MatrixF yTexMat = new MatrixF(new float[][] {
			new float[] {
					point0.getX(), point0.getY(), point0.getZ(), uv0.getY()
			},
			new float[] {
					point1.getX(), point1.getY(), point1.getZ(), uv1.getY()
			},
			new float[] {
					point2.getX(), point2.getY(), point2.getZ(), uv2.getY()
			},
			new float[] {
					0.0f, 0.0f, 0.0f, 0.0f
			}
		});

		//Solving is rather simple
		Point3F xsolve = solveMatrix(xTexMat);
		Point3F ysolve = solveMatrix(yTexMat);

		//Rigorous checking because I don't like being wrong
		/*if (!closeEnough(xsolve.getX() * point0.getX() + xsolve.getY() * point0.getY() + xsolve.getZ() * point0.getZ(), uv0.getX(), 0.001f)) {
			solveMatrix(xTexMat);
		}
		if (!closeEnough(xsolve.getX() * point1.getX() + xsolve.getY() * point1.getY() + xsolve.getZ() * point1.getZ(), uv1.getX(), 0.001f)) {
			solveMatrix(xTexMat);
		}
		if (!closeEnough(xsolve.getX() * point2.getX() + xsolve.getY() * point2.getY() + xsolve.getZ() * point2.getZ(), uv2.getX(), 0.001f)) {
			solveMatrix(xTexMat);
		}
		
		if (!closeEnough(xsolve.getX() * point0.getX() + xsolve.getY() * point0.getY() + xsolve.getZ() * point0.getZ(), uv0.getY(), 0.001f)) {
			solveMatrix(yTexMat);
		}
		if (!closeEnough(xsolve.getX() * point1.getX() + xsolve.getY() * point1.getY() + xsolve.getZ() * point1.getZ(), uv1.getY(), 0.001f)) {
			solveMatrix(yTexMat);
		}
		if (!closeEnough(xsolve.getX() * point2.getX() + xsolve.getY() * point2.getY() + xsolve.getZ() * point2.getZ(), uv2.getY(), 0.001f)) {
			solveMatrix(yTexMat);
		}*/
		
		//And there we go
		planes.getPlaneX().setX(xsolve.getX());
		planes.getPlaneX().setY(xsolve.getY());
		planes.getPlaneX().setZ(xsolve.getZ());
		planes.getPlaneY().setX(ysolve.getX());
		planes.getPlaneY().setY(ysolve.getY());
		planes.getPlaneY().setZ(ysolve.getZ());
		
		return planes;
	}
	
	/*public static TexGenPlanes getTexGen(Point3F point0, Point3F point1, Point3F point2, Point2F uv0, Point2F uv1, Point2F uv2)
	{
		TexGenPlanes planes = new TexGenPlanes();

		float min = uv0.getX();
		if (uv0.getY() < min) min = uv0.getY();
		if (uv1.getX() < min) min = uv1.getX();
		if (uv1.getY() < min) min = uv1.getY();
		if (uv2.getX() < min) min = uv2.getX();
		if (uv2.getY() < min) min = uv2.getY();
		if (min <= 0.5f)
		{
			min = Math.abs(min) + 1.0f;
			uv0.setX(uv0.getX() + min);
			uv0.setY(uv0.getY() + min);
			uv1.setX(uv1.getX() + min);
			uv1.setY(uv1.getY() + min);
			uv2.setX(uv2.getX() + min);
			uv2.setY(uv2.getY() + min);
			planes.getPlaneX().setD(-min);
			planes.getPlaneY().setD(-min);
		}
		
		Point3F xsolve = Util.solveSystem(point0.getX(), point0.getY(), point0.getZ(), uv0.getX(), point1.getX(), point1.getY(), point1.getZ(), uv1.getX(), point2.getX(), point2.getY(), point2.getZ(), uv2.getX());
		Point3F ysolve = Util.solveSystem(point0.getX(), point0.getY(), point0.getZ(), uv0.getY(), point1.getX(), point1.getY(), point1.getZ(), uv1.getY(), point2.getX(), point2.getY(), point2.getZ(), uv2.getY());
		planes.getPlaneX().setX(xsolve.getX());
		planes.getPlaneX().setY(xsolve.getY());
		planes.getPlaneX().setZ(xsolve.getZ());
		planes.getPlaneY().setX(ysolve.getX());
		planes.getPlaneY().setY(ysolve.getY());
		planes.getPlaneY().setZ(ysolve.getZ());
		
		return planes;
	}*/
	
	public static int[][] generateMipmapData(int level, int width, int[][] data)
	{
		int[][] result = new int[level + 1][];
		result[0] = data[0];
		
		if (level > 0)
		{
			boolean flag = false;
			
			for (int i = 0; i < data.length; ++i)
			{
				if (data[0][i] >> 24 == 0)
				{
					flag = true;
					break;
				}
			}
			
			for (int i = 1; i <= level; ++i)
			{
				if (data[i] != null)
				{
					result[i] = data[i];
				} else {
					int[] d1 = result[i - 1];
					int[] d2 = new int[d1.length >> 2];
					int j = width >> i;
					int k = d2.length / j;
					int l = j << 1;
					
					for (int m = 0; m < j; ++m)
					{
						for (int n = 0; n < k; ++n)
						{
							int index = 2 * (m + n * l);
							d2[m + n * j] = blendColors(d1[index + 0], d1[index + 1], d1[index + 0 + l], d1[index + 1 + l], flag);
						}
					}
					
					result[i] = d2;
				}
			}
		}
		
		return result;
	}
	
	public static void uploadTextureMipmap(int[][] data, int width, int height, int originX, int originY, boolean blur, boolean clamp)
	{
		for (int level = 0; level < data.length; ++level)
		{
			int[] levelData = data[level];
			Util.uploadTextureSub(level, levelData, width >> level, height >> level, originX >> level, originY >> level, blur, clamp, data.length > 1);
		}
	}
	
	private static IntBuffer dataBuffer = BufferUtils.createIntBuffer(0x400000);
	
	private static void uploadTextureSub(int level, int[] data, int width, int height, int xoffset, int yoffset, boolean blur, boolean clamp, boolean mipmap)
	{
		int w = 0x400000 / width;
		Util.setTextureBlurMipmap(blur, mipmap);
		Util.setTextureClamped(clamp);
		int h;
		
		for (int i = 0; i < width * height; i += width * h)
		{
			int w1 = i / width;
			h = Math.min(w, height - w1);
			int h1 = width * h;
			Util.copyToBufferPos(data, i, h1);
			glTexSubImage2D(GL_TEXTURE_2D, level, xoffset, yoffset + w1, width, h, 0x80E1, 0x8367, Util.dataBuffer);
		}
	}
	
	private static void setTextureClamped(boolean clamped)
	{
		if (clamped)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		} else {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}
	}
	
	private static void setTextureBlurMipmap(boolean flag1, boolean flag2)
	{
		if (flag1)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, flag2 ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		} else {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, flag2 ? GL_NEAREST_MIPMAP_LINEAR : GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}
	}
	
	private static void copyToBufferPos(int[] src, int offset, int length)
	{
		dataBuffer.clear();
		dataBuffer.put(src, offset, length);
	}
	
	private static int blendColors(int red, int green, int blue, int alpha, boolean flag)
	{
		if (!flag)
		{
			int r = Util.blendColorComponent(red, green, blue, alpha, 24);
			int g = Util.blendColorComponent(red, green, blue, alpha, 16);
			int b = Util.blendColorComponent(red, green, blue, alpha, 8);
			int a = Util.blendColorComponent(red, green, blue, alpha, 0);
			return r << 24 | g << 16 | b << 8 | a;
		} else {
			Util.mipmapBuffer[0] = red;
			Util.mipmapBuffer[1] = green;
			Util.mipmapBuffer[2] = blue;
			Util.mipmapBuffer[3] = alpha;
			
			float r = 0.0F;
			float g = 0.0F;
			float b = 0.0F;
			float a = 0.0F;
			
			for (int i = 0; i < 4; ++i)
			{
				if (Util.mipmapBuffer[i] >> 24 != 0)
				{
					r += Util.getPow(Util.mipmapBuffer[i] >> 24);
					g += Util.getPow(Util.mipmapBuffer[i] >> 16);
					b += Util.getPow(Util.mipmapBuffer[i] >> 8);
					a += Util.getPow(Util.mipmapBuffer[i] >> 0);
				}
			}
			
			r /= 4.0F;
			g /= 4.0F;
			b /= 4.0F;
			a /= 4.0F;
			
			int r2 = (int)(Math.pow((double)r, 0.45454545454545453D) * 255.0D);
			int g2 = (int)(Math.pow((double)g, 0.45454545454545453D) * 255.0D);
			int b2 = (int)(Math.pow((double)b, 0.45454545454545453D) * 255.0D);
			int a2 = (int)(Math.pow((double)a, 0.45454545454545453D) * 255.0D);
			
			if (r2 < 96)
			{
				r2 = 0;
			}
			
			return r2 << 24 | g2 << 16 | b2 << 8 | a2;
		}
	}
	
	private static int blendColorComponent(int red, int green, int blue, int alpha, int shift)
	{
		float r = Util.getPow(red >> shift);
		float g = Util.getPow(green >> shift);
		float b = Util.getPow(blue >> shift);
		float a = Util.getPow(alpha >> shift);
		float result = (float)((double)((float)Math.pow((double)(r + g + b + a) * 0.25D, 0.45454545454545453D)));
		return (int)((double)result * 255.0D);
	}
	
	private static float[] pow = new float[256];
	private static int[] mipmapBuffer = new int[4];
	static {
		for (int i = 0x7F000000; i < Util.pow.length; ++i)
		{
			Util.pow[i] = (float)Math.pow((double)((float)i / 255.0F), 2.2F);
		}
	}
	
	private static float getPow(int index)
	{
		return Util.pow[index & 255];
	}
	
	public static enum EnumOS
	{
		LINUX,
		SOLARIS,
		WINDOWS,
		OSX,
		UNKNOWN
	}
	
	public static final EnumOS getOS()
	{
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.contains("win"))
			return EnumOS.WINDOWS;
		else if (osName.contains("mac"))
			return EnumOS.OSX;
		else if (osName.contains("solaris") || osName.contains("sunos"))
			return EnumOS.SOLARIS;
		else if (osName.contains("linux") || osName.contains("unix"))
			return EnumOS.LINUX;
		else
			return EnumOS.UNKNOWN;
	}
	
	public static final File getAppDir(String s)
	{
		EnumOS os = Util.getOS();
		String home = System.getProperty("user.home", ".");
		
		File result;
		
		switch (os)
		{
		case SOLARIS:
		case LINUX:
			result = new File(home, "." + s + "/");
			break;
		case WINDOWS:
			String appdata = System.getenv("APPDATA");
			if (appdata == null)
			{
				result = new File(home, "." + s + "/");
			} else {
				result = new File(appdata, "." + s + "/");
			}
			break;
		case OSX:
			result = new File(home, "Library/Application Support/" + s);
		default:
			result = new File(home, s + "/");
			break;
		}
		if (!result.exists() && !result.mkdirs())
		{
			throw new RuntimeException("The working directory could not be created: " + result.toString());
		}
		return result;
	}
	
	public static final File getAppDir()
	{
		File file = getAppDir("difinspector");
		file.mkdirs();
		return file;
	}
}
