package com.matt.difinspector.main;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.structures.ItrPaddedPoint;
import com.matt.difinspector.util.Util;

public class PointsPanel extends SubPanel
{
	private static final long serialVersionUID = -6923922097457175257L;
	
	private DefaultTableModel model;
	private JTable table;
	
	public PointsPanel()
	{
		this.setLayout(new BorderLayout());

		this.model = new DefaultTableModel();
		
		
		this.table = new JTable(model);
		this.table.setFillsViewportHeight(true);
		
		JScrollPane scroll = new JScrollPane(table);
		scroll.setColumnHeader(null);
		this.add(scroll, BorderLayout.CENTER);
	}
	
	public void update(Interior interior)
	{
		ItrPaddedPoint[] points = interior.getPoints();
		
		Vector<String> tableHeader = new Vector<String>();
		tableHeader.addElement("Index");
		tableHeader.addElement("X");
		tableHeader.addElement("Y");
		tableHeader.addElement("Z");
		
		List<List<String>> data = new ArrayList<List<String>>();
		
		if (points != null)
		{
			for (ItrPaddedPoint point : points)
			{
				List<String> l = new ArrayList<String>();
				l.add("" + point.getPoint().getX());
				l.add("" + point.getPoint().getY());
				l.add("" + point.getPoint().getZ());
				data.add(l);
			}
		}
		
		model.setDataVector(Util.stringListToStringVector(data), tableHeader);
	}
}
