package com.matt.difinspector.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.matt.difinspector.dts.DTSShape;
import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.interior.InteriorResource;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.mesh.InteriorBuilder;
import com.matt.difinspector.render.GLRender;
import com.matt.difinspector.util.Settings;

public class DifInspector
{
	private static DifInspector instance;
	
	public static final DifInspector getInstance()
	{
		return DifInspector.instance;
	}
	
	public static final void main(String[] args)
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e)
		{
			e.printStackTrace();
		}
		DifInspector.instance = new DifInspector();
		Settings.loadConfig();
	}
	
	private JFrame				frame;
	private JPanel				panel;
	private JMenuBar			menu;
	private JTabbedPane			tabbedPane;
								
	private DTSShape			dtsShape;
	private InteriorResource	interior;
	private InfoTab				infoTab;
	private DetailLevelsTab		detailLevelsTab;
	private SubObjectsTab		subObjectsTab;
								
	private GLRender			render;
	private RenderSettings		settings;
	private SurfaceEditor		surfaceEditor;
	private PointEditor			pointEditor;
	private PlaneEditor			planeEditor;
	private Selector			selector;
	
	private ProgressDialog		progressDialog;
	
	private float objScale = 0.01f;
								
	public DifInspector()
	{
		this.frame = new JFrame();
		this.frame.setTitle("Torque - Dif Inspector");
		this.frame.setSize(new Dimension(540, 400));
		this.frame.setLocationRelativeTo(null);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.panel = new JPanel();
		
		this.menu = new JMenuBar();
		this.render = new GLRender(60, 800, 600);
		this.settings = new RenderSettings(this.render);
		this.surfaceEditor = new SurfaceEditor();
		this.pointEditor = new PointEditor();
		this.planeEditor = new PlaneEditor();
		this.selector = new Selector(this.render);
		this.progressDialog = new ProgressDialog(this.frame);
		
		JMenu fileMenu = new JMenu("File");
		
		JMenuItem testBuildMenu = new JMenuItem("TEST BUILD");
		testBuildMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.testBuild();
			}
		});
		//fileMenu.add(testBuildMenu);
		
		JMenuItem openMenu = new JMenuItem("Open DIF");
		openMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.open();
			}
		});
		fileMenu.add(openMenu);
		
		JMenuItem openOBJMenu = new JMenuItem("Import OBJ");
		openOBJMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.importOBJ();
			}
		});
		fileMenu.add(openOBJMenu);
		
		JMenuItem openColladaMenu = new JMenuItem("Open Collada");
		openColladaMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.openCollada();
			}
		});
		//fileMenu.add(openColladaMenu);
		
		JMenuItem openDTSMenu = new JMenuItem("Open DTS");
		openDTSMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.openDTS();
			}
		});
		//fileMenu.add(openDTSMenu);
		
		fileMenu.addSeparator();
		JMenu exportDifMenu = new JMenu("Export DIF");
		
		JMenuItem exportTTEDif = new JMenuItem("Export MBG Dif");
		exportTTEDif.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportDif(-1);
			}
		});
		exportDifMenu.add(exportTTEDif);
		
		JMenuItem exportTGEA0Dif = new JMenuItem("Export TGEA v0 Dif");
		exportTGEA0Dif.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportDif(0);
			}
		});
		exportDifMenu.add(exportTGEA0Dif);
		
		JMenuItem exportTGEA2Dif = new JMenuItem("Export TGEA v2 Dif");
		exportTGEA2Dif.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportDif(2);
			}
		});
		exportDifMenu.add(exportTGEA2Dif);
		
		JMenuItem exportTGEA3Dif = new JMenuItem("Export TGEA v3 Dif");
		exportTGEA3Dif.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportDif(3);
			}
		});
		exportDifMenu.add(exportTGEA3Dif);
		
		JMenuItem exportTGEA4Dif = new JMenuItem("Export TGEA v4 Dif");
		exportTGEA4Dif.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportDif(4);
			}
		});
		exportDifMenu.add(exportTGEA4Dif);
		
		JMenuItem exportTGEA13Dif = new JMenuItem("Export TGEA v13 Dif");
		exportTGEA13Dif.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportDif(13);
			}
		});
		exportDifMenu.add(exportTGEA13Dif);
		
		JMenuItem exportTGEA14Dif = new JMenuItem("Export TGEA v14 Dif");
		exportTGEA14Dif.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportDif(14);
			}
		});
		exportDifMenu.add(exportTGEA14Dif);
		
		fileMenu.add(exportDifMenu);
		
		JMenuItem exportMap = new JMenuItem("Export Map");
		exportMap.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportMap();
			}
		});
		//fileMenu.add(exportMap);
		
		JMenuItem exportCsx = new JMenuItem("Export Csx");
		exportCsx.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportCsx();
			}
		});
		//fileMenu.add(exportCsx);
		
		JMenuItem exportRaw = new JMenuItem("Export Raw");
		exportRaw.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportRaw();
			}
		});
		fileMenu.add(exportRaw);
		
		fileMenu.addSeparator();
		
		JMenuItem exportDumpMenu = new JMenuItem("Dump Info");
		exportDumpMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exportDump();
			}
		});
		fileMenu.add(exportDumpMenu);
		
		fileMenu.addSeparator();
		
		JMenuItem batchOBJToDif = new JMenuItem("Batch OBJ2DIF");
		batchOBJToDif.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.batchOBJ2DIF();
			}
		});
		fileMenu.add(batchOBJToDif);

		fileMenu.addSeparator();
		
		/*
		 * JMenuItem verifyMenu = new JMenuItem("Verify BSP"); verifyMenu.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) { DifInspector.this.verify(); } });
		 * fileMenu.add(verifyMenu);
		 * 
		 * fileMenu.addSeparator();
		 */
		
		JMenuItem exitMenu = new JMenuItem("Exit");
		exitMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.exit();
			}
		});
		fileMenu.add(exitMenu);
		
		this.menu.add(fileMenu);
		
		JMenu editMenu = new JMenu("Edit");
		/*JMenuItem editSurfaceMenu = new JMenuItem("Edit Surface");
		editSurfaceMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.editSurface();
			}
		});
		editMenu.add(editSurfaceMenu);
		
		editMenu.addSeparator();*/
		
		JMenuItem selectMenu = new JMenuItem("Select Surface");
		selectMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.select();
			}
		});
		editMenu.add(selectMenu);
		
		JMenuItem importSubMenu = new JMenuItem("Import SubObject");
		importSubMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.importSubObject();
			}
		});
		editMenu.add(importSubMenu);
		
		/*JMenuItem boundsMenu = new JMenuItem("Recalculate Bounds");
		boundsMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.recalculateBounds();
			}
		});
		editMenu.add(boundsMenu);*/
		
		/*JMenuItem addMaterialMenu = new JMenuItem("Add Material");
		addMaterialMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.addMaterial();
			}
		});
		editMenu.add(addMaterialMenu);*/
		
		this.menu.add(editMenu);
		
		JMenu renderMenu = new JMenu("Render");
		
		JMenuItem previewMenu = new JMenuItem("Render Preview");
		previewMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.render();
			}
		});
		renderMenu.add(previewMenu);
		
		/*JMenuItem settingsMenu = new JMenuItem("Render Settings");
		settingsMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.renderSettings();
			}
		});
		renderMenu.add(settingsMenu);*/
		
		this.menu.add(renderMenu);
		
		JMenu helpMenu = new JMenu("Help");
		
		JMenuItem aboutMenu = new JMenuItem("About");
		aboutMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				DifInspector.this.about();
			}
		});
		helpMenu.add(aboutMenu);
		
		this.menu.add(helpMenu);
		
		this.panel.setLayout(new BorderLayout());
		
		this.tabbedPane = new JTabbedPane();
		
		this.infoTab = new InfoTab();
		this.detailLevelsTab = new DetailLevelsTab();
		this.subObjectsTab = new SubObjectsTab();
		
		this.tabbedPane.setVisible(false);
		
		this.panel.add(tabbedPane, BorderLayout.CENTER);
		
		this.frame.setJMenuBar(this.menu);
		this.frame.add(this.panel);
		this.frame.setVisible(true);
	}
	
	public void processInterior()
	{
		this.render.setInterior(this.interior);
		this.surfaceEditor.setInterior(this.interior);
		this.pointEditor.setInterior(this.interior);
		this.planeEditor.setInterior(this.interior);
		this.settings.refresh();
		this.selector.refresh();
		this.infoTab.update();
		this.detailLevelsTab.update();
		
		if (this.interior.getSubObjects().size() > 0)
			this.subObjectsTab.update();
		this.tabbedPane.setVisible(true);
		
		this.tabbedPane.removeAll();
		
		this.tabbedPane.addTab("General", this.infoTab);
		
		if (this.interior.getDetailLevels().size() > 0)
			this.tabbedPane.addTab("Detail Levels", this.detailLevelsTab);
			
		if (this.interior.getSubObjects().size() > 0)
			this.tabbedPane.addTab("Sub Objects", this.subObjectsTab);
	}
	
	public void open()
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Interior Files (.dif)", "dif"));
		
		if (!Settings.getLastOpenDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastOpenDir()));
		int option = chooser.showOpenDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null && file.exists())
		{
			this.interior = new InteriorResource();
			if (this.interior.read(file))
			{
				this.processInterior();
			} else
			{
				JOptionPane.showMessageDialog(this.frame, "Failed to load interior!");
			}
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastOpenDir(dir);
		Settings.saveConfig();
	}
	
	public void importOBJ()
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Object Model Files (.obj)", "obj"));
		
		if (!Settings.getLastOpenDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastOpenDir()));
		int option = chooser.showOpenDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null && file.exists())
		{
			Thread objLoader = new Thread(new Runnable(){
				@Override
				public void run()
				{
					DifInspector.this.interior = new InteriorResource();
					if (DifInspector.this.interior.readOBJ(file, objScale))
					{
						DifInspector.this.processInterior();
					} else
					{
						JOptionPane.showMessageDialog(DifInspector.this.frame, "Failed to load interior!");
					}
				}
			});
			objLoader.start();
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastOpenDir(dir);
		Settings.saveConfig();
	}
	
	public void openCollada()
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Collada Files (.dae)", "dae"));
		
		if (!Settings.getLastOpenDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastOpenDir()));
		int option = chooser.showOpenDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null && file.exists())
		{
			this.interior = new InteriorResource();
			if (this.interior.readCollada(file))
			{
				this.processInterior();
			} else
			{
				JOptionPane.showMessageDialog(this.frame, "Failed to load collada!");
			}
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastOpenDir(dir);
		Settings.saveConfig();
	}
	
	public void openDTS()
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("DTS Files (.dts)", "dts"));
		
		if (!Settings.getLastOpenDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastOpenDir()));
		int option = chooser.showOpenDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null && file.exists())
		{
			this.dtsShape = new DTSShape();
			if (this.dtsShape.read(file))
			{
				this.dtsShape.print();
				//this.processInterior();
			} else
			{
				JOptionPane.showMessageDialog(this.frame, "Failed to load DTS!");
			}
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastOpenDir(dir);
		Settings.saveConfig();
	}
	
	public void exportDif(int version)
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Export Failed:\n" + "No Interior Loaded!");
			return;
		}
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		String versionText = ((version == -1) ? "MBG" : ("v" + version));
		chooser.setFileFilter(new FileNameExtensionFilter("Interior " + versionText + " Files (.dif)", "dif"));
		
		if (!Settings.getLastSaveDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastSaveDir()));
		int option = chooser.showSaveDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null)
		{
			if (this.interior.write(file, version))
			{
				JOptionPane.showMessageDialog(this.frame, "Successfully Exported!");
			} else
			{
				JOptionPane.showMessageDialog(this.frame, "Export Failed:\n" + this.interior.getErrors().toString());
			}
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastSaveDir(dir);
		Settings.saveConfig();
	}
	
	public void exportMap()
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Export Failed:\n" + "No Interior Loaded!");
			return;
		}
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Map Files (.map)", "map"));
		
		if (!Settings.getLastSaveDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastSaveDir()));
		int option = chooser.showSaveDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null)
		{
			if (this.interior.exportMap(file))
			{
				JOptionPane.showMessageDialog(this.frame, "Successfully Exported Map!");
			} else
			{
				JOptionPane.showMessageDialog(this.frame, "Map Export Failed!");
			}
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastSaveDir(dir);
		Settings.saveConfig();
	}
	
	public void exportCsx()
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Export Failed:\n" + "No Interior Loaded!");
			return;
		}
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Csx Files (.csx)", "csx"));
		
		if (!Settings.getLastSaveDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastSaveDir()));
		int option = chooser.showSaveDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null)
		{
			if (this.interior.exportCsx(file))
			{
				JOptionPane.showMessageDialog(this.frame, "Successfully Exported Csx!");
			} else
			{
				JOptionPane.showMessageDialog(this.frame, "Csx Export Failed!");
			}
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastSaveDir(dir);
		Settings.saveConfig();
	}
	
	public void exportRaw()
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Export Failed:\n" + "No Interior Loaded!");
			return;
		}
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Raw Files (.raw)", "raw"));
		
		if (!Settings.getLastSaveDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastSaveDir()));
		int option = chooser.showSaveDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null)
		{
			if (this.interior.exportRaw(file))
			{
				JOptionPane.showMessageDialog(this.frame, "Successfully Exported Raw!");
			} else
			{
				JOptionPane.showMessageDialog(this.frame, "Raw Export Failed!");
			}
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastSaveDir(dir);
		Settings.saveConfig();
	}
	
	public void exportDump()
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Dump Failed:\n" + "No Interior Loaded!");
			return;
		}
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Plain Text Files (.txt)", "txt"));
		
		if (!Settings.getLastSaveDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastSaveDir()));
		int option = chooser.showSaveDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null)
		{
			if (this.interior.dumpInfo(file))
			{
				JOptionPane.showMessageDialog(this.frame, "Successfully Dumped!");
			} else
			{
				JOptionPane.showMessageDialog(this.frame, "Dump Failed!");
			}
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastSaveDir(dir);
		Settings.saveConfig();
	}
	
	public void render()
	{
		Thread renderThread = new Thread(new Runnable() {
			@Override
			public void run()
			{
				DifInspector.this.render.start();
			}
		});
		renderThread.start();
		
		if (this.interior != null)
			this.render.setInterior(this.interior);
	}
	
	public void editSurface(int surfaceId)
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Operation Failed:\n" + "No Interior Loaded!");
			return;
		}
		this.surfaceEditor.setSurface(surfaceId);
		this.surfaceEditor.setVisible(true);
	}
	
	public void editPoint(int pointId)
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Operation Failed:\n" + "No Interior Loaded!");
			return;
		}
		this.pointEditor.setPoint(pointId);
		this.pointEditor.setVisible(true);
	}
	
	public void editPlane(int planeId)
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Operation Failed:\n" + "No Interior Loaded!");
			return;
		}
		this.planeEditor.setPlane(planeId);
		this.planeEditor.setVisible(true);
	}
	
	public void select()
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Operation Failed:\n" + "No Interior Loaded!");
			return;
		}
		this.selector.setVisible(true);
		//this.render.setHighlight(true);
	}
	
	public void importSubObject()
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Operation Failed:\n" + "No Interior Loaded!");
			return;
		}
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setFileFilter(new FileNameExtensionFilter("Interior Files (.dif)", "dif"));
		
		if (!Settings.getLastOpenDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastOpenDir()));
		int option = chooser.showOpenDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File file = chooser.getSelectedFile();
		if (file != null && file.exists())
		{
			InteriorResource res = new InteriorResource();
			if (res.read(file))
			{
				Interior it = res.getDetailLevels().get(0);
				this.interior.getSubObjects().add(it);
				this.processInterior();
			} else
			{
				JOptionPane.showMessageDialog(this.frame, "Failed to load SubObject!");
			}
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastOpenDir(dir);
		Settings.saveConfig();
	}
	
	public void renderSettings()
	{
		this.settings.setVisible(true);
	}
	
	public void batchOBJ2DIF()
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setMultiSelectionEnabled(true);
		chooser.setFileFilter(new FileNameExtensionFilter("Object Model Files (.obj)", "obj"));
		
		if (!Settings.getLastOpenDir().isEmpty())
			chooser.setCurrentDirectory(new File(Settings.getLastOpenDir()));
		int option = chooser.showOpenDialog(this.frame);
		if (option != JFileChooser.APPROVE_OPTION)
			return;
		
		File[] files = chooser.getSelectedFiles();
		if (files != null && files.length > 0)
		{
			for (int i = 0; i < files.length; i++)
			{
				File file = files[i];
				if (file != null && file.exists())
				{
					Thread objLoader = new Thread(new Runnable(){
						@Override
						public void run()
						{
							InteriorResource res = new InteriorResource();
							if (res.readOBJ(file, objScale))
							{
								String p = file.toString();
								p = p.replace(".obj", ".dif");
								res.write(new File(p), 0);
							} else
							{
								JOptionPane.showMessageDialog(DifInspector.this.frame, "Failed to load interior!");
							}
						}
					});
					objLoader.start();
				}
			}
			JOptionPane.showMessageDialog(DifInspector.this.frame, "Conversion Done!");
		}
		
		String dir = chooser.getCurrentDirectory().toString();
		Settings.setLastOpenDir(dir);
		Settings.saveConfig();
	}
	
	public void about()
	{
		JOptionPane.showMessageDialog(this.frame, "Created By Matthieu Parizeau\nWith help from Anthony Kleine and Whirligig", "About", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void testBuild()
	{
		InteriorResource ir = new InteriorResource();
		
		InteriorBuilder builder = new InteriorBuilder("pickle");
		
		builder.addVertex(new Point3F(0.0f, 0.0f, 0.0f));
		builder.addVertex(new Point3F(1.0f, 1.0f, 0.0f));
		builder.addVertex(new Point3F(1.0f, 0.0f, 0.0f));
		
		Interior inter = builder.build();
		
		ir.getDetailLevels().add(inter);
		
		this.interior = ir;
		
		this.processInterior();
	}
	
	/*public void recalculateBounds()
	{
		if (this.interior == null)
		{
			JOptionPane.showMessageDialog(this.frame, "Operation Failed:\n" + "No Interior Loaded!");
			return;
		}
		
		int result = JOptionPane.showConfirmDialog(this.frame, "This is highly experimental and may break collision!\n\nAre you sure you want to do this? (NO UNDO)", "Warning", JOptionPane.YES_NO_OPTION);
		if (result == JOptionPane.YES_OPTION)
		{
			System.out.println("Recalculating Bounds");
			this.interior.recalculateBounds();
		}
	}*/
	
	public void displayProgress(boolean show)
	{
		if (show)
		{
			this.progressDialog.showDialog();
		} else {
			this.progressDialog.hideDialog();
		}
	}
	
	public void displayProgress(int progress)
	{
		this.progressDialog.setProgress(progress);
	}
	
	public void displayProgress(String status, int progress)
	{
		this.progressDialog.setStatus(status);
		this.progressDialog.setProgress(progress);
	}
	
	/*
	 * public void verify() { JFileChooser chooser = new JFileChooser();
	 * chooser.setFileSelectionMode(JFileChooser.FILES_ONLY); chooser.setFileFilter(new FileNameExtensionFilter(
	 * "Interior Files (.dif)", "dif"));
	 * 
	 * chooser.showOpenDialog(this.frame);
	 * 
	 * File file = chooser.getSelectedFile(); if (file != null && file.exists()) { InteriorResource res = new
	 * InteriorResource(); res.read(file);
	 * 
	 * if (this.interior.equals(res)) { JOptionPane.showMessageDialog(this.frame, "Files are equivalent!"); } else {
	 * JOptionPane.showMessageDialog(this.frame, "Files are different!"); } } }
	 */
	
	public void exit()
	{
		System.exit(0);
	}
	
	public InteriorResource getInterior()
	{
		return this.interior;
	}

	public GLRender getRender()
	{
		return render;
	}

	public void setRender(GLRender render)
	{
		this.render = render;
	}
	
}
