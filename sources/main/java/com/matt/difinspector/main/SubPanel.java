package com.matt.difinspector.main;

import javax.swing.JPanel;

import com.matt.difinspector.interior.Interior;

public abstract class SubPanel extends JPanel
{
	private static final long serialVersionUID = -3826897062867185671L;
	
	public SubPanel()
	{
		
	}
	
	public abstract void update(Interior interior);
}
