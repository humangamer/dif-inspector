package com.matt.difinspector.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.matt.difinspector.materials.TexData;
import com.matt.difinspector.math.Point2F;

public class SetCoordsWindow extends JFrame
{
	private static final long serialVersionUID = -6232644905968277248L;
	
	private JPanel panel;
	private TexCoordField texCoordField;
	private JButton setButton;
	private SurfaceEditor editor;
	
	public SetCoordsWindow(SurfaceEditor editor)
	{
		this.setTitle("Surface Editor");
		this.setSize(200, 165);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.panel = new JPanel();
		
		this.texCoordField = new TexCoordField();
		this.setButton = new JButton("Set");
		this.setButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				SetCoordsWindow.this.set();
			}
		});
		
		this.panel.add(this.texCoordField);
		this.panel.add(this.setButton);
		
		this.add(this.panel);
		
		this.editor = editor;
	}
	
	public void setData(TexData data)
	{
		this.texCoordField.setOffset(data.getOffset());
		this.texCoordField.setScale(data.getScale());
		this.texCoordField.setRotation(data.getRotate());
	}
	
	public void clearData()
	{
		this.texCoordField.setOffset(new Point2F());
		this.texCoordField.setScale(new Point2F());
		this.texCoordField.setRotation(0);
	}
	
	public void set()
	{
		this.editor.setTexGen(this.texCoordField.getOffset(), this.texCoordField.getScale(), this.texCoordField.getRotation());
		this.setVisible(false);
	}
}
