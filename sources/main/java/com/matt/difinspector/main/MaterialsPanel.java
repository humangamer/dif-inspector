package com.matt.difinspector.main;

import java.awt.BorderLayout;
import java.util.List;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.materials.TorqueMaterial;
import com.matt.difinspector.materials.MaterialList;
import com.matt.difinspector.util.Util;

public class MaterialsPanel extends SubPanel
{
	private static final long serialVersionUID = 8724317754516499045L;
	
	private DefaultTableModel model;
	private JTable table;
	
	public MaterialsPanel()
	{
		this.setLayout(new BorderLayout());

		this.model = new DefaultTableModel();
		
		
		this.table = new JTable(model);
		this.table.setFillsViewportHeight(true);
		
		JScrollPane scroll = new JScrollPane(table);
		scroll.setColumnHeader(null);
		this.add(scroll, BorderLayout.CENTER);
	}
	
	public void update(Interior interior)
	{
		MaterialList materialList = interior.getMaterialList();
		List<TorqueMaterial> materials = materialList.getMaterials();
		
		Vector<String> tableHeader = new Vector<String>();
		tableHeader.addElement("Index");
		tableHeader.addElement("Name");
		model.setDataVector(Util.materialToStringVector(materials), tableHeader);
	}
	
}
