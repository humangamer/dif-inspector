package com.matt.difinspector.main;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.util.Util;

public class PlanesPanel extends SubPanel
{
	private static final long serialVersionUID = -6923922097457175257L;
	
	private DefaultTableModel model;
	private JTable table;
	
	public PlanesPanel()
	{
		this.setLayout(new BorderLayout());

		this.model = new DefaultTableModel();
		
		
		this.table = new JTable(model);
		this.table.setFillsViewportHeight(true);
		
		JScrollPane scroll = new JScrollPane(table);
		scroll.setColumnHeader(null);
		this.add(scroll, BorderLayout.CENTER);
	}
	
	public void update(Interior interior)
	{
		PlaneF[] planes = interior.getPlanes();
		
		Vector<String> tableHeader = new Vector<String>();
		tableHeader.addElement("Index");
		tableHeader.addElement("X");
		tableHeader.addElement("Y");
		tableHeader.addElement("Z");
		tableHeader.addElement("D");
		
		List<List<String>> data = new ArrayList<List<String>>();
		
		if (planes != null)
		{
			for (PlaneF plane : planes)
			{
				List<String> l = new ArrayList<String>();
				l.add("" + plane.getX());
				l.add("" + plane.getY());
				l.add("" + plane.getZ());
				l.add("" + plane.getD());
				data.add(l);
			}
		}
		
		model.setDataVector(Util.stringListToStringVector(data), tableHeader);
	}
}
