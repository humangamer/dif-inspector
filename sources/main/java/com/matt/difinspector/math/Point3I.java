package com.matt.difinspector.math;

public class Point3I
{
	protected int x, y, z;
	
	public Point3I(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Point3I(int xyz)
	{
		this(xyz, xyz, xyz);
	}
	
	public Point3I()
	{
		this(0);
	}
	
	public void setX(int x)
	{
		this.x = x;
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public void setY(int y)
	{
		this.y = y;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public void setZ(int z)
	{
		this.z = z;
	}
	
	public int getZ()
	{
		return this.z;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Point3I))
			return false;
		
		Point3I other = (Point3I)o;
		
		return this.x == other.x && this.y == other.y && this.z == other.z;
	}
	
	@Override
	public String toString()
	{
		return "(Point3I){x: " + this.x + ", y: " + this.y + ", z: " + this.z + "}";
	}
}
