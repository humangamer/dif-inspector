package com.matt.difinspector.math;

public class Point2I
{
	private int x, y;
	
	public Point2I(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public Point2I(int xy)
	{
		this(xy, xy);
	}
	
	public Point2I()
	{
		this(0);
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	@Override
	public String toString()
	{
		return "(Point2I){x: " + this.x + ", y: " + this.y + "}";
	}
}
