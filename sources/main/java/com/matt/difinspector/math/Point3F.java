package com.matt.difinspector.math;

public class Point3F
{
	protected float x, y, z;
	
	public Point3F(float x, float y, float z)
	{
		this.x = x;
		//if (Float.isNaN(this.x))
		//	this.x = 0;
		this.y = y;
		//if (Float.isNaN(this.y))
		//	this.y = 0;
		this.z = z;
		//if (Float.isNaN(this.z))
		//	this.z = 0;
	}
	
	public Point3F(float xyz)
	{
		this(xyz, xyz, xyz);
	}
	
	public Point3F()
	{
		this(0);
	}
	
	public float length()
	{
		return (float) Math.sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
	}
	
	public float max()
	{
		return Math.max(this.x, Math.max(this.y, this.z));
	}
	
	public Point3F normalize()
	{
		float sq = (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
		
		if (sq != 0)
		{
			float factor = 1.0f / ((float)Math.sqrt(sq));
			this.x *= factor;
			this.y *= factor;
			this.z *= factor;
		} else {
			this.x = 0;
			this.y = 0;
			this.z = 1;
		}
		
		return this;
	}
	
	public Point3F normalized()
	{
		float sq = (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
		float x;
		float y;
		float z;
		
		if (sq != 0)
		{
			float factor = 1.0f / ((float)Math.sqrt(sq));
			x = this.x * factor;
			y = this.y * factor;
			z = this.z * factor;
		} else {
			x = 0;
			y = 0;
			z = 1;
		}
		
		return new Point3F(x, y, z);
	}
	
	public Point3F rotate(Point3F axis, float angle)
	{
		float sinAngle = (float)Math.sin(-angle);
		float cosAngle = (float)Math.cos(-angle);
		
		return this.cross(axis.mul(sinAngle)).add(this.mul(cosAngle)).add(axis.mul(this.dot(axis.mul(1 - cosAngle))));
	}
	
	public Point3F rotate(QuatF rotation)
	{
		QuatF conjugate = rotation.conjugate();
		
		QuatF w = rotation.mul(this).mul(conjugate);
		
		return new Point3F(w.x, w.y, w.z);
	}
	
	public Point3F lerp(Point3F dest, float lerpFactor)
	{
		return dest.sub(this).mul(lerpFactor).add(this);
	}
	
	public Point3F swap(float val)
	{
		float x;
		float y;
		float z;
		
		if (this.x == val)
			x = 0;
		else
			x = val;
		
		if (this.y == val)
			y = 0;
		else
			y = val;
		
		if (this.z == val)
			z = 0;
		else
			z = val;
		
		return new Point3F(x, y, z);
	}
	
	public Point3F fill(float val)
	{
		float x;
		float y;
		float z;
		
		if (this.x == 0)
			x = val;
		else
			x = this.x;
		if (this.y == 0)
			y = val;
		else
			y = this.y;
		if (this.z == 0)
			z = val;
		else
			z = this.z;
		
		return new Point3F(x, y, z);
	}
	
	public boolean hasZero()
	{
		return this.x == 0 || this.y == 0 || this.z == 0;
	}
	
	public Point3F cross(Point3F r)
	{
		float x = (this.y * r.z) - (this.z * r.y);
		float y = (this.z * r.x) - (this.x * r.z);
		float z = (this.x * r.y) - (this.y * r.x);
		
		return new Point3F(x, y, z);
	}
	
	public float dot(Point3F r)
	{
		return this.x * r.x + this.y * r.y + this.z * r.z;
	}
	
	public Point3F add(Point3F r)
	{
		return new Point3F(this.x + r.x, this.y + r.y, this.z + r.z);
	}
	
	public Point3F add(float r)
	{
		return new Point3F(this.x + r, this.y + r, this.z + r);
	}
	
	public Point3F sub(Point3F r)
	{
		return new Point3F(this.x - r.x, this.y - r.y, this.z - r.z);
	}
	
	public Point3F sub(float r)
	{
		return new Point3F(this.x - r, this.y - r, this.z - r);
	}
	
	public Point3F mul(Point3F r)
	{
		return new Point3F(this.x * r.x, this.y * r.y, this.z * r.z);
	}
	
	public Point3F mul(float r)
	{
		return new Point3F(this.x * r, this.y * r, this.z * r);
	}
	
	public Point3F div(Point3F r)
	{
		return new Point3F(this.x / r.x, this.y / r.y, this.z / r.z);
	}
	
	public Point3F div(float r)
	{
		return new Point3F(this.x / r, this.y / r, this.z / r);
	}
	
	public void setMinX(float r)
	{
		this.x = (r < this.x) ? r : this.x;
	}
	
	public void setMinY(float r)
	{
		this.y = (r < this.y) ? r : this.y;
	}
	
	public void setMinZ(float r)
	{
		this.z = (r < this.z) ? r : this.z;
	}
	
	public void setMin(Point3F r)
	{
		this.x = (r.x < this.x) ? r.x : this.x;
		this.y = (r.y < this.y) ? r.y : this.y;
		this.z = (r.z < this.z) ? r.z : this.z;
	}
	
	public void setMaxX(float r)
	{
		this.x = (r > this.x) ? r : this.x;
	}
	
	public void setMaxY(float r)
	{
		this.y = (r > this.y) ? r : this.y;
	}
	
	public void setMaxZ(float r)
	{
		this.z = (r > this.z) ? r : this.z;
	}
	
	public void setMax(Point3F r)
	{
		this.x = (r.x > this.x) ? r.x : this.x;
		this.y = (r.y > this.y) ? r.y : this.y;
		this.z = (r.z > this.z) ? r.z : this.z;
	}
	
	/*public Point3F rotate(float cx, float cy, float cz, float angle)
	{
		float sin = (float) Math.sin(angle);
		float cos = (float) Math.cos(angle);
		float tan = (float) Math.tan(angle);
		*/
		/*float x = this.x - cx;
		float y = this.y - cy;
		float z = this.z - cz;
		
		x = this.x * cos - this.y * sin;
		y = this.x * sin + this.y * cos;
		z = this.z; // todo: Z ROT*/
		/*
		if (cx != 0)
		{
			float x = this.x;
			float y = this.y * cos - this.z * sin;
			float z = this.y * sin + this.z * cos;
			
			return new Point3F(x, y, z);
		} else if (cy != 0)
		{
			float x = this.x * cos + this.z * sin;
			float y = this.y;
			float z = this.x * -sin + this.z * cos;
			
			return new Point3F(x, y, z);
		} else if (cz != 0)
		{
			float x = this.x * cos - this.y * sin;
			float y = this.x * sin + this.y * cos;
			float z = this.z;
			
			return new Point3F(x, y, z);
		}
		return null;
	}*/
	
	public float[] toArray()
	{
		return new float[]{this.x, this.y, this.z};
	}
	
	public String getPointString()
	{
		return this.x + " " + this.y + " " + this.z;
	}
	
	public Point3F set(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		
		return this;
	}
	
	public Point3F set(Point3F r)
	{
		return this.set(r.x, r.y, r.z);
	}
	
	public void setX(float x)
	{
		this.x = x;
	}
	
	public float getX()
	{
		return this.x;
	}
	
	public void setY(float y)
	{
		this.y = y;
	}
	
	public float getY()
	{
		return this.y;
	}
	
	public void setZ(float z)
	{
		this.z = z;
	}
	
	public float getZ()
	{
		return this.z;
	}
	
	public float absMax()
	{
		float x = Math.abs(this.x);
		float y = Math.abs(this.y);
		float z = Math.abs(this.z);
		
		return Math.max(x, Math.max(y, z));
	}
	
	public Point3F abs()
	{
		return new Point3F(Math.abs(this.x), Math.abs(this.y), Math.abs(this.z));
	}
	
	public Point3F toDegrees()
	{
		return new Point3F((float)Math.toDegrees(this.x), (float)Math.toDegrees(this.y), (float)Math.toDegrees(this.z));
	}
	
	public Point3F toRadians()
	{
		return new Point3F((float)Math.toRadians(this.x), (float)Math.toRadians(this.y), (float)Math.toRadians(this.z));
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Point3F))
			return false;
		
		Point3F other = (Point3F)o;
		
		return this.x == other.x && this.y == other.y && this.z == other.z;
	}
	
	@Override
	public String toString()
	{
		return "(Point3F){x: " + this.x + ", y: " + this.y + ", z: " + this.z + "}";
	}
}
