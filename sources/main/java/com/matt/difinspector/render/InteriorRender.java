package com.matt.difinspector.render;

import static com.matt.difinspector.render.GLUtil.*;
import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.matt.difinspector.interior.Interior;
import com.matt.difinspector.interior.InteriorResource;
import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point2F;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.ColorF;
import com.matt.difinspector.structures.ConvexHull;
import com.matt.difinspector.structures.ItrPaddedPoint;
import com.matt.difinspector.structures.Surface;
import com.matt.difinspector.structures.TexGenPlanes;

public class InteriorRender extends SceneObject
{
	private InteriorResource interior;
	private static Map<String, GLTexture> textures = new HashMap<String, GLTexture>();
	private int highlightIndex;
	private EnumSelectionMode surfaceHighlight;
	private ColorF highlightColor;
	private ColorF highlightColorAlt;
	private ColorF highlightColorAlt2;
	private List<RenderTri> triangles;
	//private List<RenderObj> objects;
	
	public InteriorRender(InteriorResource interior)
	{
		InteriorRender.textures.clear();
		this.interior = interior;
		
		this.highlightColor = new ColorF(0.0f, 0.0f, 1.0f, 0.75f);
		this.highlightColorAlt = new ColorF(0.0f, 1.0f, 0.0f, 0.75f);
		this.highlightColorAlt2 = new ColorF(1.0f, 0.0f, 0.0f, 0.75f);
		
		this.surfaceHighlight = EnumSelectionMode.NONE;
		this.highlightIndex = -1;
		
		//this.objects = new ArrayList<RenderObj>();
	}
	
	/*public void setHighlight(boolean highlight, boolean highlightHulls, boolean highlightSurfaces, int highlightIndex)
	{
		if (highlight)
		{
			if (highlightHulls)
				this.surfaceHighlight = EnumSelectionMode.HULLS;
			else if (highlightSurfaces)
				this.surfaceHighlight = EnumSelectionMode.SURFACES;
			
			this.highlightIndex = highlightIndex;
		} else {
			this.highlightIndex = -1;
		}
	}*/
	
	public void setSelection(EnumSelectionMode mode, int index)
	{
		this.surfaceHighlight = mode;
		this.highlightIndex = index;
		
		this.triangles = null;
	}
	
	/*public void init()
	{
		Interior it = this.interior.getDetailLevels().get(0);
		List<TorqueMaterial> materials = it.getMaterialList().getMaterials();
		
		this.textures = new GLTexture[materials.size()];
		
		for (int i = 0; i < this.textures.length; i++)
		{
			String name = materials.get(i).getName();
			if (name.equals("NULL") || name.equals("ORIGIN") || name.equals("TRIGGER") || name.equals("EMITTER"))
				continue;
			
			this.textures[i] = new GLTexture("textures/" + name);
		}
		
		this.initialized = true;
	}*/
	
	public static void addTexture(String textureName)
	{
		if (textureName.equals("NULL") || textureName.equals("ORIGIN") || textureName.equals("TRIGGER") || textureName.equals("EMITTER"))
			return;
		
		GLTexture texture = new GLTexture("textures/" + textureName);
		if (!texture.isValid())
			texture = new GLTexture("textures/" + "warnMat");
		
		InteriorRender.textures.put(textureName, texture);
	}
	
	public static void bindTexture(String textureName)
	{
		if (textureName.equals("NULL") || textureName.equals("ORIGIN") || textureName.equals("TRIGGER") || textureName.equals("EMITTER"))
		{
			glBindTexture(GL_TEXTURE_2D, 0);
			return;
		}
		
		if (InteriorRender.textures.containsKey(textureName))
		{
			GLTexture texture = InteriorRender.textures.get(textureName);
			if (texture.isValid())
			{
				InteriorRender.textures.get(textureName).bind();
			} else {
				glBindTexture(GL_TEXTURE_2D, 0);
			}
		} else {
			InteriorRender.addTexture(textureName);
			InteriorRender.bindTexture(textureName);
		}
	}
	
	@Override
	public void update()
	{
		//if (!this.initialized)
		//	this.init();
	}
	
	@Override
	public void render()
	{
		glColor(ColorF.WHITE);
		/*glBegin(GL_TRIANGLES);
		{
			glColor3f(1.0f, 0.0f, 0.0f);
			glVertex3f(0, 0, 0);
			glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(0, 1, 0);
			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(1, 1, 0);
		}
		glEnd();*/
		
		Interior interior = this.interior.getDetailLevels().get(0);
		
		ConvexHull[] hulls = interior.getConvexHulls();
		if (hulls == null)
			return;
		
		if (this.triangles == null)
		{
			this.triangles = new ArrayList<RenderTri>();
			
			List<String> textureNames = new ArrayList<String>();
		
			for (int i = 0; i < hulls.length; i++)
			{
				List<RenderTri> hullTris = new ArrayList<RenderTri>();
				ConvexHull hull = hulls[i];
				
				if (this.surfaceHighlight == EnumSelectionMode.HULLS)
				{
					if (this.highlightIndex == i)
					{
						glColor(this.highlightColor);
					} else {
						glColor(ColorF.WHITE);
					}
				}
				
				int surfaceStart = hull.getSurfaceStart();
				int surfaceCount = hull.getSurfaceCount();
				
				for (int j = surfaceStart; j < surfaceStart + surfaceCount; j++)
				//for (int j = 0; j < interior.getSurfaces().length; j++)
				{
					/*if (j >= interior.getSurfaces().length || j < 0)
					{
						System.out.println("Invalid Surface Index: " + j + " on Hull: " + i);
						continue;
					}*/
					//if (j >= interior.getHullSurfaceIndices().length)
					//	continue;
					//System.out.println(j);
					int sIndex = interior.getHullSurfaceIndices()[j];
					Surface surface = interior.getSurfaces()[sIndex];
	
					short planeIndex = surface.getPlaneIndex();
					planeIndex &= ~0x8000;
					//if (planeIndex >= interior.getPlanes().length || planeIndex < 0)
						//planeIndex = 0;
						//continue;
					PlaneF plane = interior.getPlanes()[planeIndex];
					
					Point3F normal = interior.getPlaneNormals()[plane.getNormalIndex()];
					if (surface.isPlaneFlipped())
					{
						normal = normal.mul(-1);
					}
					
					int windingStart = surface.getWindingStart();
					int windingCount = surface.getWindingCount();
					
					// Bind Texture
					String textureName = surface.getTexture();
					if (!textureNames.contains(textureName))
						textureNames.add(textureName);
					//this.bindTexture(textureName);
					
					boolean selected = false;
					
					// Set Color Of Set Texture
					if (this.surfaceHighlight == EnumSelectionMode.SURFACES)
					{
						if (this.highlightIndex == interior.getHullSurfaceIndices()[j])
						{
							//glColor(this.highlightColor);
							selected = true;
						}// else {
							//glColor(ColorF.WHITE);
						//}
					}
					
					ItrPaddedPoint[] points = interior.getPoints();
					int[] windings = interior.getWindings();
					for (int k = windingStart + 2; k < windingStart + windingCount; k++)
					{	
						Point3F p1, p2, p3;
						
						if ((k - (windingStart + 2)) % 2 == 0)
						{
							p1 = points[windings[k]].getPoint();
							p2 = points[windings[k - 1]].getPoint();
							p3 = points[windings[k - 2]].getPoint();
						} else {
							p1 = points[windings[k - 2]].getPoint();
							p2 = points[windings[k - 1]].getPoint();
							p3 = points[windings[k]].getPoint();
						}
						
						TexGenPlanes texGenEq = surface.getTexGen();
						
						Point2F uv1 = new Point2F(texGenEq.getPlaneX().getDistanceToPoint(p1), texGenEq.getPlaneY().getDistanceToPoint(p1));
						Point2F uv2 = new Point2F(texGenEq.getPlaneX().getDistanceToPoint(p2), texGenEq.getPlaneY().getDistanceToPoint(p2));
						Point2F uv3 = new Point2F(texGenEq.getPlaneX().getDistanceToPoint(p3), texGenEq.getPlaneY().getDistanceToPoint(p3));
						
						RenderTri tri = new RenderTri(selected, textureName, normal, normal, normal, p1, p2, p3, uv1, uv2, uv3);
						
						this.triangles.add(tri);
						hullTris.add(tri);
					}
				}

				/*glBegin(GL_TRIANGLES);
				
				ItrPaddedPoint[] points = interior.getPoints();
				int[] windings = interior.getWindings();
				for (int k = windingStart + 2; k < windingStart + windingCount; k++)
				{	
					Point3F p1, p2, p3;
					
					if ((k - (windingStart + 2)) % 2 == 0)
					{
						//if (windings[k] >= points.length)
							//continue;
						p1 = points[windings[k]].getPoint();
						p2 = points[windings[k - 1]].getPoint();
						p3 = points[windings[k - 2]].getPoint();
					} else {
						p1 = points[windings[k - 2]].getPoint();
						p2 = points[windings[k - 1]].getPoint();
						p3 = points[windings[k]].getPoint();
					}
					
					TexGenPlanes texGenEq = surface.getTexGen();//interior.getTexGenEQs().get(surface.getTexGenIndex());
					
					Point2F uv1 = new Point2F(texGenEq.getPlaneX().getDistanceToPoint(p1), texGenEq.getPlaneY().getDistanceToPoint(p1));
					Point2F uv2 = new Point2F(texGenEq.getPlaneX().getDistanceToPoint(p2), texGenEq.getPlaneY().getDistanceToPoint(p2));
					Point2F uv3 = new Point2F(texGenEq.getPlaneX().getDistanceToPoint(p3), texGenEq.getPlaneY().getDistanceToPoint(p3));
					
					glNormal3f(normal.getX(), normal.getY(), normal.getZ());
					glTexCoord2f(uv1.getX(), uv1.getY());
					glVertex3f(p1.getX(), p1.getY(), p1.getZ());
					glTexCoord2f(uv2.getX(), uv2.getY());
					glVertex3f(p2.getX(), p2.getY(), p2.getZ());
					glTexCoord2f(uv3.getX(), uv3.getY());
					glVertex3f(p3.getX(), p3.getY(), p3.getZ());
				}
				
				glEnd();*/
				
				// TODO: FIX THIS \/\/
				/*for (int j = 0; j < textureNames.size(); j++)
				{
					String textureName = textureNames.get(j);
					
					List<RenderTri> tris = new ArrayList<RenderTri>();
					
					for (int k = 0; k < hullTris.size(); k++)
					{
						RenderTri triangle = hullTris.get(k);
						
						boolean matches = (triangle.getTexture() == null) ? (textureName == null) : (triangle.getTexture().equals(textureName));
						
						if (matches)
							tris.add(triangle);
					}
					
					RenderObj obj = new RenderObj(textureName, tris);
					
					this.objects.add(obj);
				}*/
			}
		}
		
		/*if (this.objects != null)
		{
			for (RenderObj obj : this.objects)
				obj.render();
		}*/
		
		if (this.triangles != null)
		{
			for (RenderTri tri : this.triangles)
				tri.render();
		}
		
		if (this.surfaceHighlight == EnumSelectionMode.SURFACES)
		{
			Surface[] surfaces = interior.getSurfaces();
			ItrPaddedPoint[] points = interior.getPoints();
			glBindTexture(GL_TEXTURE_2D, 0);
			if (this.highlightIndex >= 0 && this.highlightIndex < surfaces.length)
			{
				Surface surface = surfaces[this.highlightIndex];
				

				short planeIndex = surface.getPlaneIndex();
				planeIndex &= ~0x8000;
				Point3F normal = interior.getPlanes()[planeIndex];
				
				Point3F cPoint = new Point3F();
				
				int windingStart = surface.getWindingStart();
				int windingCount = surface.getWindingCount();
				for (int i = windingStart; i < windingStart + windingCount; i++)
				{
					Point3F point = points[interior.getWindings()[i]].getPoint();
					Cube cube = new Cube(point, new Point3F(0.25f), this.highlightColorAlt);
					cube.render();
					cPoint = cPoint.add(point);
				}
				
				Point3F n1 = cPoint.div(windingCount).add(normal.mul(0.25f));
				Point3F n2 = n1.add(normal);
				
				Cube nc1 = new Cube(n1, new Point3F(0.25f), this.highlightColorAlt2);
				nc1.render();
				Cube nc2 = new Cube(n2, new Point3F(0.25f), this.highlightColor);
				nc2.render();
			}
		} else if (this.surfaceHighlight == EnumSelectionMode.POINTS)
		{
			ItrPaddedPoint[] points = interior.getPoints();
			glBindTexture(GL_TEXTURE_2D, 0);
			if (this.highlightIndex >= 0 && this.highlightIndex < points.length)
			{
				Point3F point = points[this.highlightIndex].getPoint();
				Cube cube = new Cube(point, new Point3F(0.25f), this.highlightColor);
				cube.render();
				
				/*float px = point.getX();
				float py = point.getY();
				float pz = point.getZ();
				glBegin(GL_POINTS);
				{
					glColor3f(0.0f, 0.0f, 1.0f);
					//glVertex3f(px - 0.5f, py - 0.5f, pz - 0.5f);
					glVertex3f(px, py, pz);
					
				}
				glEnd();*/
			}
		} else if (this.surfaceHighlight == EnumSelectionMode.PLANES)
		{
			PlaneF[] planes = interior.getPlanes();
			
			if (this.highlightIndex >= 0 && this.highlightIndex < planes.length)
			{
				PlaneF plane = planes[this.highlightIndex];
				
				Plane p = new Plane(plane, this.highlightColorAlt, this.highlightColor);
				p.render();
			}
		}
	}
	
	public InteriorResource getInterior()
	{
		return this.interior;
	}
}
