package com.matt.difinspector.render;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.matt.difinspector.math.Point2F;

public class Input
{
	private static boolean[] keys = new boolean[256];
	private static boolean[] mouseButtons = new boolean[3];
	
	public static void update()
	{
		for (int i = 0; i < keys.length; i++)
		{
			keys[i] = Keyboard.isKeyDown(i);
		}
		
		for (int i = 0; i < mouseButtons.length; i++)
		{
			mouseButtons[i] = Mouse.isButtonDown(i);
		}
	}
	
	public static boolean getMouseDown(int button)
	{
		return Mouse.isButtonDown(button) && !mouseButtons[button];
	}
	
	public static boolean getMouseHeld(int button)
	{
		return Mouse.isButtonDown(button);
	}
	
	public static boolean getMouseUp(int button)
	{
		return !Mouse.isButtonDown(button) && mouseButtons[button];
	}
	
	public static boolean getKeyDown(int key)
	{
		return Keyboard.isKeyDown(key) && !keys[key];
	}
	
	public static boolean getKeyHeld(int key)
	{
		return Keyboard.isKeyDown(key);
	}
	
	public static boolean getKeyUp(int key)
	{
		return !Keyboard.isKeyDown(key) && keys[key];
	}
	
	public static void setCursor(boolean show)
	{
		Mouse.setGrabbed(!show);
	}
	
	public static void setMousePosition(Point2F position)
	{
		Mouse.setCursorPosition((int)position.getX(), (int)position.getY());
	}
	
	public static Point2F getMousePosition()
	{
		return new Point2F(Mouse.getX(), Mouse.getY());
	}
	
	public static int getMouseX()
	{
		return Mouse.getX();
	}
	
	public static int getMouseY()
	{
		return Mouse.getY();
	}
}
