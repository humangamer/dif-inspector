package com.matt.difinspector.render;

import static com.matt.difinspector.render.GLUtil.*;
import static org.lwjgl.opengl.GL11.*;

import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.ColorF;

public class Plane
{
	protected PlaneF plane;
	protected ColorF colorFront;
	protected ColorF colorBack;
	
	public Plane(PlaneF plane, ColorF colorFront, ColorF colorBack)
	{
		this.plane = plane;
		this.colorFront = colorFront;
		this.colorBack = colorBack;
	}
	
	public void render()
	{	
		Point3F up = new Point3F(0, 0, 1);
		Point3F down = new Point3F(0, 0, -1);
		Point3F left = new Point3F(0, 1, 0);
		Point3F right = new Point3F(0, -1, 0);
		
		float d = this.plane.getD();
		
		Point3F plane = this.plane;
		
		if (up.equals(plane))
		{
			up = new Point3F(1, 0, 0);
		} else if (down.equals(plane))
		{
			up = new Point3F(-1, 0, 0);
		}
		
		if (left.equals(plane))
		{
			left = new Point3F(1, 0, 0);
		} else if (right.equals(plane))
		{
			left = new Point3F(-1, 0, 0);
		}

		glBegin(GL_QUADS);
		{
			// HACKY HACK Y U NO WERK
			if (plane.getX() > 0)
			{
				// Side 1
				glColor(this.colorBack);
				glVertex(plane.cross(up).mul(10).sub(plane.mul(d)));
				glVertex(plane.cross(left).mul(10).sub(plane.mul(d)));
				glVertex(up.cross(plane).mul(10).sub(plane.mul(d)));
				glVertex(left.cross(plane).mul(10).sub(plane.mul(d)));
				
				// Side 2
				glColor(this.colorFront);
				glVertex(left.cross(plane).mul(10).sub(plane.mul(d)));
				glVertex(up.cross(plane).mul(10).sub(plane.mul(d)));
				glVertex(plane.cross(left).mul(10).sub(plane.mul(d)));
				glVertex(plane.cross(up).mul(10).sub(plane.mul(d)));
			} else {
				// Side 1
				glColor(this.colorFront);
				glVertex(plane.cross(up).mul(10).sub(plane.mul(d)));
				glVertex(plane.cross(left).mul(10).sub(plane.mul(d)));
				glVertex(up.cross(plane).mul(10).sub(plane.mul(d)));
				glVertex(left.cross(plane).mul(10).sub(plane.mul(d)));
				
				// Side 2
				glColor(this.colorBack);
				glVertex(left.cross(plane).mul(10).sub(plane.mul(d)));
				glVertex(up.cross(plane).mul(10).sub(plane.mul(d)));
				glVertex(plane.cross(left).mul(10).sub(plane.mul(d)));
				glVertex(plane.cross(up).mul(10).sub(plane.mul(d)));
			}
		}
		glEnd();
	}
}
