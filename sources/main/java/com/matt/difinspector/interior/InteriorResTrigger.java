package com.matt.difinspector.interior;

import java.io.BufferedWriter;
import java.io.IOException;

import com.matt.difinspector.io.ReverseDataInputStream;
import com.matt.difinspector.io.ReverseDataOutputStream;
import com.matt.difinspector.math.PlaneF;
import com.matt.difinspector.math.Point3F;
import com.matt.difinspector.structures.Edge;
import com.matt.difinspector.structures.Polyhedron;

public class InteriorResTrigger
{
	private String name;
	private String dataBlock;
	private InteriorDictionary dictionary;
	private Point3F offset;
	private Polyhedron polyhedron;
	
	public InteriorResTrigger()
	{
		
	}
	
	public boolean read(ReverseDataInputStream dis) throws IOException
	{
		this.name = dis.readString();
		this.dataBlock = dis.readString();
		
		this.dictionary = new InteriorDictionary();
		this.dictionary.read(dis);
		
		int numPolyPoints = dis.readInt();
		Point3F[] pointList = new Point3F[numPolyPoints];
		for (int i = 0; i < numPolyPoints; i++)
		{
			pointList[i] = dis.readPoint3F();
		}
		
		int numPolyPlanes = dis.readInt();
		PlaneF[] planeList = new PlaneF[numPolyPlanes];
		for (int i = 0; i < numPolyPlanes; i++)
		{
			planeList[i] = dis.readPlaneF();
		}
		
		int numEdges = dis.readInt();
		Edge[] edgeList = new Edge[numEdges];
		for (int i = 0; i < numEdges; i++)
		{
			int face1 = dis.readInt();
			int face2 = dis.readInt();
			int vertex1 = dis.readInt();
			int vertex2 = dis.readInt();
			edgeList[i] = new Edge(vertex1, vertex2, face1, face2);
		}
		
		this.polyhedron = new Polyhedron(pointList, planeList, edgeList);
		
		this.offset = dis.readPoint3F();
		
		return true;
	}
	
	public boolean write(ReverseDataOutputStream dos)
	{
		// TODO
		return false;
	}
	
	public void dumpInfo(BufferedWriter bw) throws IOException
	{
		bw.write("<InteriorResTrigger>");
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getDataBlock()
	{
		return this.dataBlock;
	}
	
	public InteriorDictionary getDictionary()
	{
		return this.dictionary;
	}
	
	public Point3F getOffset()
	{
		return this.offset;
	}
	
	public Polyhedron getPolyhedron()
	{
		return this.polyhedron;
	}
}
