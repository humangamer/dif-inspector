package com.matt.difinspector.interior;

public enum EnumSubObjectType
{
	TranslucentSubObjectKey,
	MirrorSubObjectKey;
}
