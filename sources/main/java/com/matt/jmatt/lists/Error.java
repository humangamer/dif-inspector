package com.matt.jmatt.lists;

/**
 * Standard Error
 * @author Matthieu Parizeau
 */
public class Error
{
	/**
	 * Error Message
	 */
	protected String message;
	
	/**
	 * Error Details
	 */
	protected String details;
	
	/**
	 * Initializes a new error with a message and details
	 * @param message error message
	 * @param details error details
	 */
	public Error(String message, String details)
	{
		// Set the message and details
		this.message = message;
		this.details = details;
	}
	
	/**
	 * Initializes a new error with a message and no details
	 * @param message error message
	 */
	public Error(String message)
	{
		// Set the message
		this(message, "");
	}
	
	/**
	 * Initializes a new unknown error
	 */
	public Error()
	{
		// Set the message to unknown error as a message was not specified
		this("Unknown Error");
	}
	
	/**
	 * Gets the error message
	 * @return error message
	 */
	public String getMessage()
	{
		// Return the error message
		return this.message;
	}
	
	/**
	 * Gets the error details
	 * @return error details
	 */
	public String getDetails()
	{
		// Return the error details
		return this.details;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Error)
		{
			// If were comparing to an error
			Error other = (Error)o;
			
			// Check if the message and details are equal
			return this.message.equals(other.message) && this.details.equals(other.details);
		} else if (o instanceof String)
		{
			// If were comparing to a string
			String other = (String)o;
			
			// Convert this error to a string and check that it is equal to the other
			return this.toString().equals(other);
		} else {
			// If were comparing to something else, it is definitely not equal
			return false;
		}
	}
	
	@Override
	public String toString()
	{
		// If there are no details, just return the message
		if (this.details == null || this.details.isEmpty())
			return this.message;
		
		// Otherwise return the message and the details
		return this.message + ": " + this.details;
	}
}