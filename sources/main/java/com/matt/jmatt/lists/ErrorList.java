package com.matt.jmatt.lists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Standard list of error
 * @author Matthieu Parizeau
 */
public class ErrorList implements Iterable<Error>
{
	/**
	 * Underlying error list
	 */
	protected List<Error> errors;
	
	/**
	 * Initializes a new error list
	 */
	public ErrorList()
	{
		// Initialize the underlying error list
		this.errors = new ArrayList<Error>();
	}
	
	/**
	 * Adds an error to the list
	 * @param error error to add
	 */
	public void add(Error error)
	{
		// Add an error directly
		this.errors.add(error);
	}
	
	/**
	 * Adds an error to the list
	 * @param message error message
	 * @param details error details
	 */
	public void add(String message, String details)
	{
		// Add a new error with a message and details
		this.add(new Error(message, details));
	}
	
	/**
	 * Adds an error to the list
	 * @param message error message
	 */
	public void add(String message)
	{
		// Add a new error with a message
		this.add(new Error(message));
	}
	
	/**
	 * Adds an unknown error to the list
	 */
	public void add()
	{
		// Add a new error with no arguments
		this.add(new Error());
	}
	
	/**
	 * Gets an error
	 * @param index error index
	 * @return error at specified index
	 */
	public Error get(int index)
	{
		// Get the error at the specified index and return it
		return this.errors.get(index);
	}
	
	/**
	 * Gets an error as a string
	 * @param index error index
	 * @return error at specified index as string
	 */
	public String getErrorString(int index)
	{
		// Get the error at the specified index and return it as a string
		return this.errors.get(index).toString();
	}
	
	/**
	 * Gets all the errors as an array
	 * @return error array
	 */
	public Error[] getErrors()
	{
		// Create a new error array
		Error[] result = new Error[this.errors.size()];
		
		// Convert the underlying error list to an array
		this.errors.toArray(result);
		
		// Return the error array
		return result;
	}
	
	/**
	 * Gets all the errors as a string array
	 * @return error string array
	 */
	public String[] getErrorStrings()
	{
		// Create a new string array
		String[] result = new String[this.errors.size()];
		
		// Loop through the error list
		for (int i = 0; i < this.errors.size(); i++)
		{
			// Add the error to the string array
			result[i] = this.errors.get(i).toString();
		}
		
		// Return the string array
		return result;
	}
	
	/**
	 * Gets the number of errors
	 * @return error count
	 */
	public int getCount()
	{
		// Pass the call to the underlying list
		return this.errors.size();
	}
	
	/**
	 * Appends another list of errors to this one
	 * @param errorList error list to append
	 */
	public void append(ErrorList errorList)
	{
		// Loop through the errors in the other list
		for (Error e : errorList)
		{
			// Add them to this list
			this.add(e);
		}
	}
	
	/**
	 * Clears all the errors
	 */
	public void clear()
	{
		this.errors.clear();
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		// Loop through the error list
		for (int i = 0; i < this.errors.size(); i++)
		{
			// Append the error to the string
			sb.append(this.errors.get(i).toString());
			
			// As long as this is not the last element, add a new line
			if (i < this.errors.size() - 1)
				sb.append("\n");
		}
		
		// Return the error list as a string
		return sb.toString();
	}

	@Override
	public Iterator<Error> iterator()
	{
		return new Iterator<Error>() {
			
			/**
			 * Current Index
			 */
			protected int index;
			
			@Override
			public Error next()
			{
				// Get the error at the current index
				Error error = ErrorList.this.get(index);
				
				// Increment index
				this.index++;
				
				// Return the error
				return error;
			}
			
			@Override
			public boolean hasNext()
			{
				// There are more errors as long as the index is less than the error count
				return this.index < ErrorList.this.getCount();
			}
		};
	}
}
